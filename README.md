1、连接仓库 1)、2)都可以
```
　　1） git clone git@xxx
　　2） mkdir test
　　　　cd test
　　　　git init
　　　　git remote add origin git@xxx
```
2、拉取最新分支 git pull origin master（develop、release） 

3、切换到开发分支 git checkout develop

4、develop分支切出本地分支(分支名：yangwenzhi) git checkout -b yangwenzhi

5、修改代码

6、查看修改记录 git status

7、对比本地与仓库代码差异 git diff 文件路径

8、添加文件到索引 git add 文件路径

9、提交文件到仓库 git commit -m "描述"

10、推送文件到远程仓库 git push origin yangwenzhi （不需要长时间保留可以省去这步）

11、合并本地分支到develop
```
　　1）切换到develop分支 git checkout develop
　　2）拉取develop最新代码，减少合并时冲突 git pull origin develop
　　3）git merge yangwenzhi 合并yangwenzhi分支到develop分支
```
12、 推送合并后的develop分支到远程仓库

13、 上线流程
```
　　1）合并develop分支到release分支
　　2）服务器上执行上线脚本：sh ./publish_musiclive.sh 1
　　3）服务器上执行回滚脚本：sh ./rollback_musiclive.sh musiclive_20180111_2 （需要回滚时执行）
```

14、 Jenkins上线流程
```
　　1）git checkout develop
　　2）git pull origin develop
　　3）git checkout release
　　4）git pull origin relese
　　5）git merge develop
　　6）git push origin release
　　7）打开http://172.16.1.32:8080/jenkins/view/fe/job/musiclive-shell/
　　8）点击立即构建，查看Console Output日志是否显示publish finish!
```