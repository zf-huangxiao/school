"use strict";

window.base = {
    //获取url参数
    getUrlParam: function getUrlParam(name) {
        var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)"); //构造一个含有目标参数的正则表达式对象
        var r = window.location.search.substr(1).match(reg); //匹配目标参数
        if (r != null) {
            return r[2];
        } else {
            return null; //返回参数值
        }
    },
    // 格式化数量（最多显示到9999，超出显示1.2w，末位向下取整。1w不显示为1.0w）
    formatNum: function formatNum(num, radix) {
        return num > 9999 ? parseInt(num / radix * 10) / 10 + 'w' : num;
    }
};

;(function (win) {
    function initNavActive() {
        var $curIndex = localStorage.getItem('$curIndex');
        var $navList = $('.nav-list');
        if ($curIndex === null) return;
        var $curNav = $navList.find('li').eq($curIndex);
        if ($curNav.hasClass('selected')) return;
        $curNav.addClass('selected').siblings().removeClass('selected');
        $curNav.siblings().find('i').removeClass('bounceInLeft');
    }
    win.initNavActive = initNavActive;
})(window);

var webInit = {
    init: function init() {
        window.initNavActive();
    }
};
var events = {
    init: function init() {}
};
var variables = {};

$(document).ready(function () {
    // "use strict";
    webInit.init();
    // events.init();
});

$(document).ready(function () {
    (function () {
        var handleNavHover,
            handleNavClick,
            $body = $('html,body'),
            $nav = $('.navs'),
            $lessons = $('.courses-box'),
            $teachers = $('.teachers-con'),
            $curIndex;
        var navs = [{
            url: './index.html',
            text: '首页'
        }, {
            url: './index.html#lessons',
            text: '课程'
        }, {
            url: './index.html#teachers',
            text: '讲师'
        }, {
            url: './contact.html',
            text: '联系我们'
        }];

        handleNavHover = function handleNavHover(e) {
            if ($(this).hasClass('selected')) return;
            if (e.type === 'mouseenter') {
                $(this).find('i').addClass('bounceInLeft');
            } else if (e.type === 'mouseleave') {
                $(this).find('i').removeClass('bounceInLeft');
            }
        };
        handleNavClick = function handleNavClick(e) {
            if ($(this).hasClass('selected')) return;
            $curIndex = $(this).index();
            localStorage.setItem('$curIndex', $curIndex);
            if ($(this).hasClass('nav-courses')) {
                //点击‘课程’
                switch ($lessons.length) {
                    case 1:
                        $(this).addClass('selected').siblings().removeClass('selected');
                        $(this).siblings().find('i').removeClass('bounceInLeft');
                        $body.animate({ scrollTop: $lessons.offset().top + 'px' }, 500, "swing");
                        break;
                    case 0:
                        window.open(navs[$curIndex].url);
                        break;
                }
            } else if ($(this).hasClass('nav-teachers')) {
                //点击‘教师’
                switch ($teachers.length) {
                    case 1:
                        $(this).addClass('selected').siblings().removeClass('selected');
                        $(this).siblings().find('i').removeClass('bounceInLeft');
                        $body.animate({ scrollTop: $teachers.offset().top + 'px' }, 500, "swing");
                        break;
                    case 0:
                        window.open(navs[$curIndex].url);
                        break;
                }
            } else {
                window.open(navs[$curIndex].url);
            }
        };
        $nav.on('mouseenter mouseleave', 'li', handleNavHover);
        $nav.on('click', 'li', handleNavClick);
    })();
});