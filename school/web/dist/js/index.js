"use strict";

window.base = {
    //获取url参数
    getUrlParam: function getUrlParam(name) {
        var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)"); //构造一个含有目标参数的正则表达式对象
        var r = window.location.search.substr(1).match(reg); //匹配目标参数
        if (r != null) {
            return r[2];
        } else {
            return null; //返回参数值
        }
    },
    // 格式化数量（最多显示到9999，超出显示1.2w，末位向下取整。1w不显示为1.0w）
    formatNum: function formatNum(num, radix) {
        return num > 9999 ? parseInt(num / radix * 10) / 10 + 'w' : num;
    }
};

// doT.js
// 2011, Laura Doktorova, https://github.com/olado/doT
// Licensed under the MIT license.

(function () {
    "use strict";

    var doT = {
        version: '1.0.1',
        templateSettings: {
            evaluate: /\{\{([\s\S]+?(\}?)+)\}\}/g,
            interpolate: /\{\{=([\s\S]+?)\}\}/g,
            encode: /\{\{!([\s\S]+?)\}\}/g,
            use: /\{\{#([\s\S]+?)\}\}/g,
            useParams: /(^|[^\w$])def(?:\.|\[[\'\"])([\w$\.]+)(?:[\'\"]\])?\s*\:\s*([\w$\.]+|\"[^\"]+\"|\'[^\']+\'|\{[^\}]+\})/g,
            define: /\{\{##\s*([\w\.$]+)\s*(\:|=)([\s\S]+?)#\}\}/g,
            defineParams: /^\s*([\w$]+):([\s\S]+)/,
            conditional: /\{\{\?(\?)?\s*([\s\S]*?)\s*\}\}/g,
            iterate: /\{\{~\s*(?:\}\}|([\s\S]+?)\s*\:\s*([\w$]+)\s*(?:\:\s*([\w$]+))?\s*\}\})/g,
            varname: 'it',
            strip: true,
            append: true,
            selfcontained: false
        },
        template: undefined, //fn, compile template
        compile: undefined //fn, for express
    },
        global;

    if (typeof module !== 'undefined' && module.exports) {
        module.exports = doT;
    } else if (typeof define === 'function' && define.amd) {
        define(function () {
            return doT;
        });
    } else {
        global = function () {
            return this || (0, eval)('this');
        }();
        global.doT = doT;
    }

    function encodeHTMLSource() {
        var encodeHTMLRules = { "&": "&#38;", "<": "&#60;", ">": "&#62;", '"': '&#34;', "'": '&#39;', "/": '&#47;' },
            matchHTML = /&(?!#?\w+;)|<|>|"|'|\//g;
        return function () {
            return this ? this.replace(matchHTML, function (m) {
                return encodeHTMLRules[m] || m;
            }) : this;
        };
    }
    String.prototype.encodeHTML = encodeHTMLSource();

    var startend = {
        append: { start: "'+(", end: ")+'", endencode: "||'').toString().encodeHTML()+'" },
        split: { start: "';out+=(", end: ");out+='", endencode: "||'').toString().encodeHTML();out+='" }
    },
        skip = /$^/;

    function resolveDefs(c, block, def) {
        return (typeof block === 'string' ? block : block.toString()).replace(c.define || skip, function (m, code, assign, value) {
            if (code.indexOf('def.') === 0) {
                code = code.substring(4);
            }
            if (!(code in def)) {
                if (assign === ':') {
                    if (c.defineParams) value.replace(c.defineParams, function (m, param, v) {
                        def[code] = { arg: param, text: v };
                    });
                    if (!(code in def)) def[code] = value;
                } else {
                    new Function("def", "def['" + code + "']=" + value)(def);
                }
            }
            return '';
        }).replace(c.use || skip, function (m, code) {
            if (c.useParams) code = code.replace(c.useParams, function (m, s, d, param) {
                if (def[d] && def[d].arg && param) {
                    var rw = (d + ":" + param).replace(/'|\\/g, '_');
                    def.__exp = def.__exp || {};
                    def.__exp[rw] = def[d].text.replace(new RegExp("(^|[^\\w$])" + def[d].arg + "([^\\w$])", "g"), "$1" + param + "$2");
                    return s + "def.__exp['" + rw + "']";
                }
            });
            var v = new Function("def", "return " + code)(def);
            return v ? resolveDefs(c, v, def) : v;
        });
    }

    function unescape(code) {
        return code.replace(/\\('|\\)/g, "$1").replace(/[\r\t\n]/g, ' ');
    }

    doT.template = function (tmpl, c, def) {
        c = c || doT.templateSettings;
        var cse = c.append ? startend.append : startend.split,
            needhtmlencode,
            sid = 0,
            indv,
            str = c.use || c.define ? resolveDefs(c, tmpl, def || {}) : tmpl;

        str = ("var out='" + (c.strip ? str.replace(/(^|\r|\n)\t* +| +\t*(\r|\n|$)/g, ' ').replace(/\r|\n|\t|\/\*[\s\S]*?\*\//g, '') : str).
        //.replace(/'|\\/g, '\\//##include('../../../vv-base/js/doT.js')')
        replace(c.interpolate || skip, function (m, code) {
            return cse.start + unescape(code) + cse.end;
        }).replace(c.encode || skip, function (m, code) {
            needhtmlencode = true;
            return cse.start + unescape(code) + cse.endencode;
        }).replace(c.conditional || skip, function (m, elsecase, code) {
            return elsecase ? code ? "';}else if(" + unescape(code) + "){out+='" : "';}else{out+='" : code ? "';if(" + unescape(code) + "){out+='" : "';}out+='";
        }).replace(c.iterate || skip, function (m, iterate, vname, iname) {
            if (!iterate) return "';} } out+='";
            sid += 1;indv = iname || "i" + sid;iterate = unescape(iterate);
            return "';var arr" + sid + "=" + iterate + ";if(arr" + sid + "){var " + vname + "," + indv + "=-1,l" + sid + "=arr" + sid + ".length-1;while(" + indv + "<l" + sid + "){" + vname + "=arr" + sid + "[" + indv + "+=1];out+='";
        }).replace(c.evaluate || skip, function (m, code) {
            return "';" + unescape(code) + "out+='";
        }) + "';return out;").replace(/\n/g, '\\n').replace(/\t/g, '\\t').replace(/\r/g, '\\r').replace(/(\s|;|\}|^|\{)out\+='';/g, '$1').replace(/\+''/g, '').replace(/(\s|;|\}|^|\{)out\+=''\+/g, '$1out+=');

        if (needhtmlencode && c.selfcontained) {
            str = "String.prototype.encodeHTML=(" + encodeHTMLSource.toString() + "());" + str;
        }
        try {
            return new Function(c.varname, str);
        } catch (e) {
            if (typeof console !== 'undefined') console.log("Could not create a template function: " + str);
            throw e;
        }
    };

    doT.compile = function (tmpl, def) {
        return doT.template(tmpl, null, def);
    };
})();

;(function (win) {
    function initNavActive() {
        var $curIndex = localStorage.getItem('$curIndex');
        var $navList = $('.nav-list');
        if ($curIndex === null) return;
        var $curNav = $navList.find('li').eq($curIndex);
        if ($curNav.hasClass('selected')) return;
        $curNav.addClass('selected').siblings().removeClass('selected');
        $curNav.siblings().find('i').removeClass('bounceInLeft');
    }
    win.initNavActive = initNavActive;
})(window);

var baseURL = location.protocol + "//" + location.hostname;
var webInit = {
    init: function init() {
        window.initNavActive();
        this.getBanners();
        this.getTeachersList();
    },
    initSwiperBanner: function initSwiperBanner() {
        var swiperBanner = new Swiper('.banner', {
            // direction: 'vertical', // 垂直切换选项
            loop: true,
            pagination: {
                el: '.swiper-pagination'
            },
            autoplay: 5000,
            effect: 'fade',
            prevButton: '.swiper-button-prev',
            nextButton: '.swiper-button-next',
            onSlideChangeEnd: function onSlideChangeEnd(swiper) {
                var index = swiper.activeIndex; //切换结束时，告诉我现在是第几个slide
                webInit.lazyLoad($(swiper.slides[index]).find('img').attr('data-url'));
                // console.log($(swiper.slides[index]).find('img').attr('data-url'),'===========')
            }
        });
    },
    initSwipeTeachers: function initSwipeTeachers() {
        var swiperBanner = new Swiper('.teachers', {
            loop: true,
            pagination: {
                el: '.swiper-pagination'
            },
            autoplay: 5000,
            prevButton: '.swiper-button-prev',
            nextButton: '.swiper-button-next',
            effect: 'coverflow',
            slidesPerView: 3,
            centeredSlides: true,
            coverflow: {
                rotate: 25,
                stretch: 10,
                depth: 60,
                modifier: 3,
                slideShadows: true
            }
        });
    },
    getBanners: function getBanners() {
        var _this2 = this;

        $.get(baseURL + "/data/header.json", function (res) {
            console.log('banner:', res);
            if (!res || !res.length) return;
            var doTtem = doT.template($('#banner-tem').html());
            variables.$bannerCon.find('.swiper-wrapper').append(doTtem(res));
            _this2.initSwiperBanner();
        });
    },
    getTeachersList: function getTeachersList() {
        var _this3 = this;

        $.get(baseURL + "/data/teacher.json", function (res) {
            if (!res || !res.length) return;
            var doTtem = doT.template($('#teacher-tem').html());
            variables.$teacherCon.find('.swiper-wrapper').append(doTtem(res));
            _this3.initSwipeTeachers();
            console.log('老师:', res);
        });
    },
    lazyLoad: function lazyLoad(url) {
        var _this = this;
        if (this.loadedBanner.indexOf(url) >= 0) return; //图片已加载过
        var img = document.createElement('img');
        img.src = url;
        img.onload = function () {
            variables.$bannerCon.find(".swiper-slide img[data-url=\"" + url + "\"]").attr({ src: url, style: 'width:100%;height:100%;margin:0;' });
            _this.loadedBanner.push(url);
        };
    },

    loadedBanner: []

};
var events = {
    init: function init() {
        "use strict";
    }
};
(function (win) {
    var variables = {
        $nav: $('.nav-list'),
        $bannerCon: $('.banner'),
        $teacherCon: $('.teachers-con'),
        coursesOffT: $('.courses-box').offset().top + 'px'
    };
    variables.teachersOffT = variables.$teacherCon.offset().top + 'px', win.variables = variables;
})(window);

$(document).ready(function () {
    "use strict";

    webInit.init();
    events.init();
});
$(document).ready(function () {
    (function () {
        var handleNavHover,
            handleNavClick,
            $body = $('html,body'),
            $nav = $('.navs'),
            $lessons = $('.courses-box'),
            $teachers = $('.teachers-con'),
            $curIndex;
        var navs = [{
            url: './index.html',
            text: '首页'
        }, {
            url: './index.html#lessons',
            text: '课程'
        }, {
            url: './index.html#teachers',
            text: '讲师'
        }, {
            url: './contact.html',
            text: '联系我们'
        }];

        handleNavHover = function handleNavHover(e) {
            if ($(this).hasClass('selected')) return;
            if (e.type === 'mouseenter') {
                $(this).find('i').addClass('bounceInLeft');
            } else if (e.type === 'mouseleave') {
                $(this).find('i').removeClass('bounceInLeft');
            }
        };
        handleNavClick = function handleNavClick(e) {
            if ($(this).hasClass('selected')) return;
            $curIndex = $(this).index();
            localStorage.setItem('$curIndex', $curIndex);
            if ($(this).hasClass('nav-courses')) {
                //点击‘课程’
                switch ($lessons.length) {
                    case 1:
                        $(this).addClass('selected').siblings().removeClass('selected');
                        $(this).siblings().find('i').removeClass('bounceInLeft');
                        $body.animate({ scrollTop: $lessons.offset().top + 'px' }, 500, "swing");
                        break;
                    case 0:
                        window.open(navs[$curIndex].url);
                        break;
                }
            } else if ($(this).hasClass('nav-teachers')) {
                //点击‘教师’
                switch ($teachers.length) {
                    case 1:
                        $(this).addClass('selected').siblings().removeClass('selected');
                        $(this).siblings().find('i').removeClass('bounceInLeft');
                        $body.animate({ scrollTop: $teachers.offset().top + 'px' }, 500, "swing");
                        break;
                    case 0:
                        window.open(navs[$curIndex].url);
                        break;
                }
            } else {
                window.open(navs[$curIndex].url);
            }
        };
        $nav.on('mouseenter mouseleave', 'li', handleNavHover);
        $nav.on('click', 'li', handleNavClick);
    })();
});