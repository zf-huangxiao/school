;(function (win) {
    function initNavActive() {
        const $curIndex = localStorage.getItem('$curIndex');
        const $navList = $('.nav-list');
        if ($curIndex === null) return;
        const $curNav = $navList.find('li').eq($curIndex);
        if ($curNav.hasClass('selected')) return;
        $curNav.addClass('selected').siblings().removeClass('selected');
        $curNav.siblings().find('i').removeClass('bounceInLeft');
    }
    win.initNavActive = initNavActive;
})(window);
