$(document).ready(function () {
    (function () {
        var  handleNavHover,
            handleNavClick,
            $body =  $('html,body'),
            $nav = $('.navs'),
            $lessons = $('.courses-box'),
            $teachers = $('.teachers-con'),
            $curIndex;
        const navs = [
            {
                url:'./index.html',
                text:'首页'
            },
            {
                url:'./index.html#lessons',
                text:'课程'
            },
            {
                url:'./index.html#teachers',
                text:'讲师'
            },
            {
                url:'./contact.html',
                text:'联系我们'
            }
        ];



        handleNavHover = function (e) {
            if ($(this).hasClass('selected')) return;
            if (e.type === 'mouseenter') {
                $(this).find('i').addClass('bounceInLeft');
            } else if (e.type === 'mouseleave') {
                $(this).find('i').removeClass('bounceInLeft');
            }
        };
        handleNavClick = function (e) {
            if($(this).hasClass('selected')) return;
            $curIndex = $(this).index();
            localStorage.setItem('$curIndex',$curIndex);
            if ($(this).hasClass('nav-courses')) {
                //点击‘课程’
                switch ($lessons.length){
                    case 1:
                        $(this).addClass('selected').siblings().removeClass('selected');
                        $(this).siblings().find('i').removeClass('bounceInLeft');
                        $body.animate({scrollTop: $lessons.offset().top + 'px',}, 500, "swing");
                        break;
                    case 0:
                        window.open(navs[$curIndex].url);
                        break;
                }
            } else if ($(this).hasClass('nav-teachers')) {
                //点击‘教师’
                switch ($teachers.length){
                    case 1:
                        $(this).addClass('selected').siblings().removeClass('selected');
                        $(this).siblings().find('i').removeClass('bounceInLeft');
                        $body.animate({scrollTop: $teachers.offset().top + 'px'}, 500, "swing");
                        break;
                    case 0:
                        window.open(navs[$curIndex].url);
                        break;
                }
            }else{
                window.open(navs[$curIndex].url);
            }

        };
        $nav.on('mouseenter mouseleave', 'li', handleNavHover);
        $nav.on('click', 'li', handleNavClick);
    })();
});