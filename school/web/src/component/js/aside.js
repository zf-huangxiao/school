const baseURL = `${location.protocol}//${location.hostname}`;
const webInit = {
    init(){
        window.initNavActive();
        this.getBanners();
        this.getTeachersList();
    },
    initSwiperBanner(){
        var swiperBanner = new Swiper('.banner', {
            // direction: 'vertical', // 垂直切换选项
            loop: true,
            pagination: {
                el: '.swiper-pagination',
            },
            autoplay: 5000,
            effect: 'fade',
            prevButton: '.swiper-button-prev',
            nextButton: '.swiper-button-next',
            onSlideChangeEnd: function(swiper){
                var index = swiper.activeIndex;//切换结束时，告诉我现在是第几个slide
                webInit.lazyLoad($(swiper.slides[index]).find('img').attr('data-url'));
                // console.log($(swiper.slides[index]).find('img').attr('data-url'),'===========')
            }
        })
    },
    initSwipeTeachers(){
        var swiperBanner = new Swiper('.teachers', {
            loop: true,
            pagination: {
                el: '.swiper-pagination',
            },
            autoplay: 5000,
            prevButton: '.swiper-button-prev',
            nextButton: '.swiper-button-next',
            effect: 'coverflow',
            slidesPerView: 3,
            centeredSlides: true,
            coverflow: {
                rotate: 25,
                stretch: 10,
                depth: 60,
                modifier: 3,
                slideShadows: true
            }
        })
    },
    getBanners(){
        $.get(`${baseURL}/data/header.json`,
            res =>{
                console.log('banner:',res);
                if(!res || !res.length) return;
                const doTtem = doT.template($('#banner-tem').html());
                variables.$bannerCon.find('.swiper-wrapper').append(doTtem(res));
                this.initSwiperBanner();

            });
    },
    getTeachersList(){
        $.get(
            `${baseURL}/data/teacher.json`,
            res =>{
                if(!res || !res.length) return;
                const doTtem = doT.template($('#teacher-tem').html());
                variables.$teacherCon.find('.swiper-wrapper').append(doTtem(res));
                this.initSwipeTeachers();
                console.log('老师:',res);
            });
    },
    lazyLoad(url){
        var _this = this;
        if (this.loadedBanner.indexOf(url) >= 0) return; //图片已加载过
        var img = document.createElement('img');
        img.src = url;
        img.onload =function () {
            variables.$bannerCon.find(`.swiper-slide img[data-url="${url}"]`).attr({src:url,style:'width:100%;height:100%;margin:0;'});
            _this.loadedBanner.push(url);
        }
    }
    ,
    loadedBanner:[],


};
const events = {
    init(){
        "use strict";

    },

};
(function (win) {
    var variables = {
        $nav: $('.nav-list'),
        $bannerCon:$('.banner'),
        $teacherCon:$('.teachers-con'),
        coursesOffT: $('.courses-box').offset().top + 'px',
    };
    variables.teachersOffT = variables.$teacherCon.offset().top + 'px',

    win.variables = variables;
})(window);



$(document).ready(function () {
    "use strict";
    webInit.init();
    events.init();
});