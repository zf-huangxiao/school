var chat = require('./chat.js');
var endList = require('./endList.js');
module.exports = function() {
	//初始化的一些参数
	var layId = '666'; //flash调用js时用到的唯一id
	//站点切换地址(rtmp视频应用)  视频质量默认表情
	//直播流
	if (golobalV.playState == 1) {
		var videoQualityData = {
			// 默认data ： 1
			add: [ //不同质量视频的跳转url
				{
					name: '标清',
					url: golobalV.rtmpUrl,
					data : 1
				}, //（注意：name和data都是可以手动配置的）
				{
					name: '高清',
					url: golobalV.rtmpUrl2,
					data : 2
				}, {
					name: '流畅',
					url: golobalV.rtmpUrl3,
					data : 3
				}
			]
		};
		var videoUrl = golobalV.rtmpUrl2;
	} else if (golobalV.playState == 4) { //回放流
		var videoQualityData = {
			add: [ //不同质量视频的跳转url
				{
					name: '标清',
					url: golobalV.vodUrl,
					data : 1
				}, //（注意：name和data都是可以手动配置的）
				{
					name: '高清',
					url: golobalV.vodUrl2,
					data : 2
				}, {
					name: '流畅',
					url: golobalV.vodUrl3,
					data : 3
				}
			]
		};
		var videoUrl = golobalV.vodUrl2;
		// var videoUrl = "http://192.168.16.204/vod/test/pt.mp4";
	}
	//var videoUrl = 'M0qTMD1VkZQ5lveXmwtUlsAWkwEYkdJVJDhVLHAanwQWMdpULHAa';//视频地址(加密的地址)
	//var videoUrl = 'http://192.168.11.168/vod/test/20160728663042.mp4'; //4:3的视频
	// var videoUrl = 'http://yc43.data.mvbox.cn/video/replay/20160913/20160913664086.mp4'; //16:9的视频
	$(document).ready(function() {
		window.create = function(){
			vv.player.create('play', layId, vv.player.lastest.swfPath, {

				layId: "666", //flash调用js时用到的唯一id
				//用户验证用的,如果视频地址是加密的，需要传参验证
				// aid: "0",//购买用户的唯一标识
				sid: golobalV.user_id, //用户唯一标识
				time: +new Date(), //当前时间
				key: "90fd6bd0c17f737ffec3055d0beebf42", //密钥
				// roomId: golobalV.roomId,
				//视频配置
				videoType: golobalV.playState == 1 ? "rtmpLive" : (golobalV.vodUrl2.match(/m3u8/gi) ? "hls" : "http"), //视频类型rtmp ,http ,rtmpLive,hls,hlsLive
				volume: "0.5", //设置默认音量
				url: videoUrl, //视频播放地址
				autoPlay: "true", //视频自动播放,如果是false,则需要点击按钮才能播放
				isEncrypt: "false", //url地址是否需要解密(如果是加密的话，必须为true)
				playfromTime: "0", //设置播放时间,注意:rtmpLive模式下无效
				muted: "false", //静音控制
				tagDate: m_d_h_m, //回放视频开始播放时间
				showTag: 1,	//0,不加载。1,加载 //2,3等等，以后扩展用
				//控制条和视频区域参数
				controlBarAutoHide: "true", //控制条是否自动隐藏
				controlBarAutoHideTime: "4", //控制条自动隐藏的时间
				controlBarType: "0", //控制条类型
				controlBarHeight: "35", //控制条高度
				videoWidth: "863", //视频区域宽度
				videoHeight: "588", //视频区域高度
				width: 863, //播放器的宽度
				height: 588, //播放器的高度
				videoBackgroundImage: "../livePC/images/play-end-bg.jpg", //播放器背景图
				//片头广告配置(视频或者图片)
				isPlugin: "false", //是否加载片头广告
				pluginTime: "10", //片头默认时间（如果是视频则取视频的时间，如果是图片取这个时间）
				pluginContentUrl: "", //片头地址（视频或是图片）
				pluginBlankUrl: "http://www.51vv.com", //片头点击后的跳转地址
				pluginAutoPlay: "true", //广告是否自动播放
				pluginBackImage: "", //片头备用图片,读取片头地址报错时用到
				//水印地址配置
				markImageUrl: "", //用户唯一标识水印图
				//聊天,弹幕
				bulletScreenShow: "true", //是否添加弹幕功能
				// nodeServer: "wss://114.112.43.81/" + golobalV.chatWsBase + "/ws/msg.htm?roomId=" + golobalV.roomId + "&lastMsgId=0", //聊天服务器
				nodeServer: "wss://zb.51vv.com/"+golobalV.chatWsBase+"/ws/msg.htm?roomId=" + golobalV.roomId,
				//是否启用选择不同质量视频的功能
				videoQualityChangeShow: golobalV.playState == 1 ? "true" : "false" //选择视频质量
			});
		}
		//直播或者回放去初始化播放器
		if (golobalV.playState == 1 || golobalV.playState == 4) {
			create();
		}
	});
	/** 这个函数在flash加载完成之后会被调用,然后就js可以回调flash了  **/
	window.playerReady = function() {
			if(golobalV.isCourse == 1 && golobalV.isbuy == "false"){
				// $("#play").css("display","none");
				$(".live-secret-box").css("display","block");
			}
			addVideoQualityData(); //flash初始化完毕后,把要选择的视频数据注入
			// alert(!window.WebSocket)
			if (!window.WebSocket) {
				chatSocketConn();
			}
			window.getCurPlayTime = function(){
				return thisMovie("pcPlayer" + layId).fl_getCurrTime();	
			}
			// alert(typeof thisMovie("pcPlayer" + layId).fl_play)
		}
	//flash上报
	window.flashSetCollectionsData = function(data) {
		//alert(data.value.playurl);
		// console.log(434534)
		data.module = "vvliveflash";
		// console.log(data);
		$.ajax({
			url: window.location.protocol + "//zhibo.stat.ubeibei.cn/livestatistics",
			type: "post",
			data: {
				"module": JSON.stringify(data),
				"vv-prod": "vvliveflash"
			},
			success: function(data) {

			}
			// headers: {					
			// 	// 'Access-Control-Allow-Origin':'*',
			// 	"vv_prod" : "vvliveflash"
			// }

		})

	}
		/** flash埋点数据---记录用户的所有操作行为和网络信息  **/
	// window.flashSetCollectionsData = function(val) {
	// 	//val是json对象
	// 	if (window.videoSetCollectionsData) {
	// 		window.videoSetCollectionsData(val);
	// 	}
	// };
	/** 播放器收到websocket消息后告知js  **/
	window.handleWebSocketMessage = function(val, swfId) {
		chat.getMsg(val);
	};
	/** 播放器里的报错消息告知js  
	201:服务器连接失败,202:服务器连接被拒绝,203:服务器已关闭,206:播放视频流失败,208:获取视频流超时,209:websocket服务器连接失败,210:websocket服务器连接被关闭**/
	window.flashErrorCall = function(val, swfId) {
		if(val == 206){
			$("#play").hide();
			$(".shang").hide();
			endList($(".live-end-list"));
			$(".live-end-icon").css("left","120px");
			$(".live-end-text").css("text-indent","40px");
			$(".live-end-text").text("这个视频不存在了，看看其他的也不错哦~");
			$(".live-end-box").css("display", "block");
		}
	};
	/** 播放器里的成功消息告知js  101:视频服务器连接成功,102:websocket服务器连接成功**/
	window.flashOKCall = function(val, swfId) {
		// alert(val)
		// console.log(val + ',' + swfId);
	};
	/** 播放器广告播放完毕后告知js  **/
	window.adsPluginStopCall = function(val, swfId) {
		// console.log(val + ',' + swfId);
	};
	/** 视频播放完毕后告知js  **/
	window.videoStopCall = function(val, swfId) {
		$("#play").hide();
		$(".live-secret-box").css("display","none");
		endList($(".play-end-list"));
		$(".play-end-box").css("display", "block");
		$(".play-end-watch-replay").click(function(){
			$(".play-end-box").css("display", "none");
			$("#play").css("display","block");
			// thisMovie("pcPlayer"+layId).fl_play(0);
			create();
		})
	};
	/**添加视频设置信息**/
	function addVideoQualityData() {
		thisMovie("pcPlayer" + layId).fl_addVideoQualityData(videoQualityData);
	};
	/**连接webSocket**/
	function chatSocketConn() {
		// thisMovie("pcPlayer" + layId).fl_chatSocketConn();
		var cookie = "wx_zb_devId="+getCookie("wx_zb_devId");
		var serverUrl = "";
		thisMovie("pcPlayer"+layId).fl_chatSocketConn(serverUrl,cookie);	
	};
	/**关闭webSocket**/
	function chatSocketClose() {
		thisMovie("pcPlayer" + layId).fl_chatSocketClose();
	};
	/**发送聊天消息**/
	window.sendChatMsg = function() {
		// alert("aa")
		var time = +new Date(),
			roomId = golobalV.roomId,
			msg = $(".input-area").val().replace(/[ ]+/, "");
		if (msg == "赶快和大家一起互动吧" || msg == "发送太频繁了，休息一下吧！" || msg == "") {
			return;
		}
		// console.log("发送消息");
		// 消息发送频率限制	      解除限制：1：超过5秒 2.发送其他消息
		var obj = {
			"time": time,
			"msg": msg
		}
		chat.sendMsgList.push(obj);
		// console.log(chat.sendMsgList)

		if (chat.sendMsgList.length >= 4) {
			var len = chat.sendMsgList.length;
			var arr = chat.sendMsgList;
			// 5s内连发三条一样的消息  
			// console.log((arr[len - 1]["msg"] == arr[len - 2]["msg"])&& (arr[len - 2]["msg"] == arr[len - 3]["msg"]))
			if ((arr[len - 1]["msg"] == arr[len - 2]["msg"]) && (arr[len - 2]["msg"] == arr[len - 3]["msg"]) && (arr[len - 3]["msg"] == arr[len - 4]["msg"]) && (arr[len - 1]["time"] - arr[len - 3]["time"] < 5000)) {
				$("#letter-num").text(40);
				$(".input-area").unbind("focus").bind("focus", function() {
					$(".input-area").val("");
				})
				$(".input-area").val("发送太频繁了，休息一下吧！").blur();
				// $(".input-area").unbind("blur").bind("blur",function(){
				// 	$(".input-area").val("赶快和大家一起互动吧");
				// })
				return;
			}
		}
		// console.log(JSON.stringify({
		// 	"type": "sendMsg",
		// 	"data": {
		// 		"roomId": roomId,
		// 		"msg": msg
		// 	}
		// }))
		thisMovie("pcPlayer" + layId).fl_sendChatMsg(JSON.stringify({
			"type": "sendMsg",
			"data": {
				"roomId": roomId,
				"msg": msg
			}
		}));
		$(".input-area").val("").focus();
	};
	/**获取当前flash**/
	window.thisMovie = function(movieName) {
		if (navigator.appName.indexOf("Microsoft") != -1) {
			return window[movieName];
		} else {
			return document[movieName];
		}
	};
}