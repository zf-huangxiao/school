/**
 * 时间格式化
 * @param  {[type]} timestamp [时间戳]
 * @return {[type]}  
 */
function getDateDiff(timestamp) {
    if(!timestamp) return;
    var liveStartYear = new Date(parseInt(timestamp)).getFullYear();
    var liveStartMonth = new Date(parseInt(timestamp)).getMonth() + 1;
    var liveStartDay = new Date(parseInt(timestamp)).getDate();
    var liveStartTime = new Date(parseInt(timestamp)).getTime();

    var nowMonth = new Date().getMonth() + 1;
    var nowTime = new Date().getTime();

    // 不同月份 显示具体年月日
    if (nowMonth - liveStartMonth > 0) {
        return liveStartYear + "年" + liveStartMonth + "月" + liveStartDay + "日";
    } else {
        // 同一个月份 
        // 超过60s
        if ((nowTime - liveStartTime) / 1000 > 60) {
            // 超过60分钟
            if ((nowTime - liveStartTime) / 1000 / 60 > 60) {
                // 超过24小时
                if ((nowTime - liveStartTime) / 1000 / 60 > 24 * 60) {
                    return Math.floor((nowTime - liveStartTime) / 1000 / 60 / 60 / 24) + "天前";
                } else {
                    return Math.floor((nowTime - liveStartTime) / 1000 / 60 / 60) + "小时前";
                }
            } else {
                return Math.floor((nowTime - liveStartTime) / 1000 / 60) + "分钟前";
            }
        } else {
            return "刚刚";
        }
    }
}

module.exports = getDateDiff;
