/**
 * 获取等级前缀图标
 */
var getLevelImg = {
    //生成认证图标
    getSign: function(level) {
        var lev = +level;
        var zoom = [17, 33, 49, 65, 81, 97, 113, 129, 145, 161, 177, 193, 209, 225, 241, 257, 273, 289, 305, 321, 337, 353, 369, 385];
        var img = '';
        for (var i = 0; i < zoom.length; i++) {
            if (lev >= zoom[zoom.length - 1]) {
                img = 'http://cdn.file.51vv.com/vvmobilelive/pc/ico/385-400.png';
                break;
            }
            if (lev >= zoom[i] && lev < zoom[i + 1]) {
                img = 'http://cdn.file.51vv.com/vvmobilelive/pc/ico/' + zoom[i] + '-' + --zoom[i + 1] + '.png';
                break;
            }
        }
        return img;
    },
    // 获取配置图片地址前缀
    getCfgImgUrl: function(callBack) {
        var getCfgInfoObj = {
            type: 'https://music.51vv.com/user/cfg/getCfgInfo.htm',
            parameter: '',
            base: location.href,
            callback: 'getCfgInfoCallback'
        };
        window.getCfgInfoCallback = function(data) {
            if (data.result == 0 && data.familyVipImgPrefix) {
                callBack(data.familyVipImgPrefix);
            }
        }
        QCefClient.invokeMethod('server', JSON.stringify(getCfgInfoObj));
    },
    //生成图标
    getLevel: function(level) {
        return 'http://cdn.file.51vv.com/vvmobilelive/pc/level/' + level + '.png';
    },
}

module.exports = getLevelImg;