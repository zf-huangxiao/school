// 字符串解析为DOM对象
function loadXMLString(str) {
    var div = document.createElement("div");
    if(typeof str == "string") div.innerHTML = str;
    return div.childNodes;
};

// 插入字符串DOM
function insertAtCaret(id, val) {
    //光标位置  光标在表情前面--上一个文本的结尾   光标在表情后面--输入框子节点的序号
    var selection = window.getSelection && window.getSelection();
    var anchorNode = selection.anchorNode;
    var anchorOffset = selection.anchorOffset;
    //输入框
    // var pn = document.getElementById(id);
    var pn = id;
    //选区
    var ran = selection.getRangeAt(0);
    //插入的表情
    var imgNode = loadXMLString(val)[0];
    if (anchorNode.nodeType == 3) {
        //文本中插入
        // if (selection.anchorOffset == anchorNode.nodeValue.length){
        //     //在文本结尾
        //     pn.insertBefore(imgNode ,anchorNode.nextElementSibling);
        // } else 
        if (selection.anchorOffset == 0){
            //在文本开头
            pn.insertBefore(imgNode ,anchorNode);
        } else {
            //在文本中
            //拆分后半部分文本并插入
            var n2 = document.createTextNode(anchorNode.nodeValue.substring(selection.anchorOffset));
            pn.replaceChild(n2, anchorNode);
            //在后半部分文本前插入表情
            imgNode = pn.insertBefore(imgNode, n2);
            //插入前半部分文本
            if(anchorOffset){
                var n1 = document.createTextNode(anchorNode.nodeValue.substring(0, anchorOffset));
                pn.insertBefore(n1, imgNode);
            }
        }
    }else if(anchorNode.nodeType == 1){
        //输入框子节点插入
        //在选中节点的后面一个节点，在它之前插入
        pn.insertBefore(imgNode, pn.childNodes[anchorOffset]);
    }
    //结尾表情被隐藏处理176
    if (imgNode.offsetLeft - pn.scrollLeft > 176){
        pn.scrollLeft = imgNode.offsetLeft - 176;
    }
    //设置焦点
    ran = ran.cloneRange();
    ran.setStartAfter(imgNode);
    selection.removeAllRanges();
    selection.addRange(ran);
    pn.focus();
};

module.exports.loadXMLString = loadXMLString;
module.exports.insertAtCaret = insertAtCaret;
