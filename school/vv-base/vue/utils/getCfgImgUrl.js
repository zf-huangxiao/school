//
/**
 * [获取配置图片地址前缀]
 * @type {Object}
 * suyue
 */
function getCfgImgUrl(callBack) {
  var getCfgInfoObj = {
    type: 'https://music.51vv.com/user/cfg/getCfgInfo.htm',
    parameter: '',
    base: location.href,
    callback: 'getCfgInfoCallback'
  };
  window.getCfgInfoCallback = function(data) {
    if (data.result == 0 && data.familyVipImgPrefix) {
      callBack(data.familyVipImgPrefix);
    }
  }
  QCefClient.invokeMethod('server', JSON.stringify(getCfgInfoObj));
}

module.exports = getCfgImgUrl;