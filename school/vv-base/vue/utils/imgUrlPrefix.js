/**
 * erxia
 * 获取图片url前缀
 */
var imgUrlPrefix = {
    // 获取配置图片地址前缀
    imgUrlPrefixFn: function(callBack) {
        // 客户端协议
        var systemObj = {
            type:'getUserSystemConfig',
            parameter: '',
            base:location.href,
            callback:'imgUrlPrefixCb'
        };
        window.imgUrlPrefixCb = function (data) {
            console.log(data);
            if(data.result == 0 && data.allImgPrefix){
                callBack(data.allImgPrefix);
            }
        };
        QCefClient.invokeMethod('client',JSON.stringify(systemObj));
        // http请求
        // var getCfgInfoObj = {
        //     type: 'https://music.51vv.com/user/cfg/getCfgInfo.htm',
        //     parameter: '',
        //     base: location.href,
        //     callback: 'imgUrlPrefixCb'
        // };
        // window.imgUrlPrefixCb = function(data) {
        //     if (data.result == 0 && data.allImgPrefix) {
        //         callBack(data.allImgPrefix);
        //     }
        // }
        // QCefClient.invokeMethod('server', JSON.stringify(getCfgInfoObj));
    }
}
module.exports = imgUrlPrefix;