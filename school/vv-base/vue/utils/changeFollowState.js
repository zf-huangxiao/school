/**
 * erxia
 * 关注操作方法
 * userId: 登录用户id,
 * toUserId: 被关注的用户id,
 * type: 类型 1关注 2取消关注
 * followStateCallback: 改变关注状态成功的回调函数
 * blacklistCallback: 黑名单关系中我被拉黑和相互拉黑的回调函数
 */
var changeFollowState = {
    init: function (userId,toUserId,type,followStateCallback,blacklistCallback) {
        switch(type)
        {
        case 1:
            // 关注
            changeFollowState.blacklistFn(userId,toUserId,type,followStateCallback,blacklistCallback);
            break;
        case 2:
            // 取消关注
            changeFollowState.followStateFn(userId,toUserId,type,followStateCallback);
            break;
        default:
            break;
        }
    },
    blacklistFn: function (userId,toUserId,type,followStateCallback,blacklistCallback) {
        // 黑名单关系
        var obj = {
            type: 'https://music.51vv.com/sod',
            parameter: '?action=33001&parameter={userID:'+ userId +',toUserID:'+ toUserId +'}',
            base: location.href,
            callback: 'blacklistCb'
        };
        window.blacklistCb = function (res) {
            console.log(res);
            // retCode 1000成功 relation 类型 0没关系 1被我拉黑 2我被拉黑 3相互拉黑
            if (res.retCode == 1000) {
                if (res.relation == 0 || res.relation == 1) {
                    // 关注
                    changeFollowState.followStateFn(userId,toUserId,type,followStateCallback);
                } else if (res.relation == 2 || res.relation == 3) {
                    // 被拉黑 提示对方设置了隐私
                    blacklistCallback(res);
                } 
            }
        };
        console.log(JSON.stringify(obj));
        window.QCefClient.invokeMethod('client',JSON.stringify(obj));
    },
    // 关注和取消关注
    followStateFn: function (userId,toUserId,type,followStateCallback) {
        var obj = {
            type: 'https://music.51vv.com/sod',
            parameter: '?action=29&parameter={userID:'+ userId +',toUserID:'+ toUserId +',type:'+ type +'}',
            base: location.href,
            callback: 'followStateCb'
        };
        window.followStateCb = function (res) {
            console.log(res);
            // retCode 1000成功 relation 类型 0未关注 1已关注 2好友
            if (res.retCode == 1000) {
                followStateCallback(res);
            }
        }.bind(this);
        console.log(JSON.stringify(obj));
        window.QCefClient.invokeMethod('server',JSON.stringify(obj));
    }
}
module.exports = changeFollowState;