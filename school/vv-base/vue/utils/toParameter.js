/**
 * [把对象转化为url参数]
 * @type {Object}
 * suyue
 */
function toParameter(obj) {
    var parameter = '';
    for (var key in obj) {
        parameter = parameter + key + '=' + obj[key] + '&';
    }
    parameter = parameter.substring(0, parameter.length - 1);
    return '?' + parameter;
}
module.exports = toParameter;