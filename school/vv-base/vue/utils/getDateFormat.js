/**
 * 时间格式化
 * 少于1分钟：xx秒前
 * 少于1小时：xx分钟前
 * 少于1天：xx小时前
 * 多与1天：MM-dd hh:mm
 * 多与1年：yyy-MM-dd
 
 * @param  {[type]} timestamp [时间戳]
 * @return {[type]}  
 */
import FormatDate from './formatDate.js';
function getDateFormat (timestamp){
 	if(!timestamp) return;
	var timeNow = parseInt(new Date().getTime()/1000);
	var now = new Date(timestamp).getTime()/1000;
	var time = timeNow - now;
	//一年后的时间
	var oneYears =  (parseInt(FormatDate(timestamp,'yyyy'))+1)+'-'+FormatDate(timestamp,'MM-dd hh:mm:ss');
	var oneTime = new Date(oneYears).getTime()/1000;
	var d_days = parseInt(time/86400);       
	var d_hours = parseInt(time/3600);       
	var d_minutes = parseInt(time/60);  
	if(time <= 0){
	 	return '刚刚';
	}else if(time <= 60){
	 	return parseInt(time) + '秒前';
	}else if(d_hours <= 0 && d_minutes > 0){       
	 	return d_minutes + "分钟前";       
	}else if(d_days <= 0 && d_hours > 0){       
	 	return d_hours + "小时前";       
	}else if(timeNow <= oneTime && d_days > 0 ){
	 	return FormatDate(timestamp,'MM-dd hh:mm')
	}else{
	 	return FormatDate(timestamp,'yyy-MM-dd')
	}
}
module.exports = getDateFormat;
