/*
* 表情渲染方法
* erxia
*/ 
function renderEmoji(msg,list,url) {
	// 如果是数字，转为字符串
    var msg = msg + '';
	// 替换信息中的 < & 空格
    msg = msg.replace(/&/g,'&amp;').replace(/\s/g,'&nbsp;').replace(/</g,'&lt;').replace(/>/g,'&gt;');
    // 如果有at信息，特殊显示
    // 提取消息中的图片信息
    msg = msg.replace(/\[[^\[\]]+\]/g,function(v,i,s){return list[v]?('<img class="emoji" src="'+ url + list[v] +'">'):v});
    return msg;
}
module.exports = renderEmoji;