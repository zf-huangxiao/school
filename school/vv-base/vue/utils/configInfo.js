/**
 * erxia
 * 获取配置信息
 */
function configInfo (callBack) {
    var systemObj = {
        type:'getUserSystemConfig',
        parameter: '',
        base:location.href,
        callback:'configInfoCb'
    };
    window.configInfoCb = function (res) {
        console.log(res);
        if (res.result == 0) {
            callBack(res);
        }
    };
    QCefClient.invokeMethod('client',JSON.stringify(systemObj));
}
module.exports = configInfo;