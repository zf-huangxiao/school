/**
 * 时间格式化
 *少于1分钟：xx秒
 *少于1小时：xx分钟（取整数，如2分钟xx秒，显示3分钟）
 *少于1天：xx小时xx分钟（取整数）
 *超过1天：xx小时xx分钟（取整数）
 * @param  {[type]} time [毫秒]
 * @return {[type]}  
 */

function formatTime(_time){
    var m; 
    var time = _time/1000;
    var d_hours = Math.floor(time/3600);       
    var d_minutes = Math.ceil(time/60);
    if(time <= 60){
        return time + '秒';
    }else if(d_hours <= 0 && d_minutes > 0){       
        return d_minutes + "分钟";       
    }else{       
        m =  Math.ceil(time%3600/60);
        return m < 60 ? (d_hours + '小时' + m + '分钟') : (d_hours+1) + '小时' ;    
    }
}

module.exports = formatTime;
