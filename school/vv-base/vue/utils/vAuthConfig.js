/*
 * erxia
 * 获取v认证配置信息
*/
function vAuthConfig(callBack) {
    var systemObj = {
        type:'https://music.51vv.com/sod',
        parameter: '?action=68001',
        base:location.href,
        callback:'vAuthConfigCb'
    };
    window.vAuthConfigCb = function (res) {
        console.log(res);
        callBack(res);
    };
    QCefClient.invokeMethod('client',JSON.stringify(systemObj));
}
module.exports = vAuthConfig;