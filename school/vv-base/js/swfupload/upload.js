// 封面
var upload1 = {
    fn1: null,
    fn2: null,
    // 初始化
    init: function(obj) {
        var settings = {
            // 基本配置
            flash_url: "../../../../vv-base/js/swfupload/swfupload.swf",
            upload_url: "http://mp-up.51vv.com/upload_v_article",
            file_size_limit: "2 MB",
            file_types: obj.flieType,
            file_types_description: "All Files",
            file_upload_limit: 0,
            file_queue_limit: 1,
            debug: false,

            // Button settings
            button_image_url: obj.btnImg,
            button_width: obj.btnWidth,
            button_height: obj.btnHeight,
            button_placeholder_id: obj.btnId,
            button_cursor: SWFUpload.CURSOR.HAND,
            button_action: SWFUpload.BUTTON_ACTION.SELECT_FILES,

            // 定义上传完成的事件
            file_queued_handler: this.fileQueued,
            file_queue_error_handler: this.fileQueueError,
            file_dialog_complete_handler: this.fileDialogComplete,
            upload_success_handler: this.uploadSuccess,
            upload_error_handler: this.uploadError
        };
        var swfu = new SWFUpload(settings);
        this.fn1 = obj.fn1;
        this.fn2 = obj.fn2;
        return swfu;
    },
    // 当一个文件被添加到上传队列时会触发此事件，提供的唯一参数为包含该文件信息的file object对象
    fileQueued: function(fileObj) {
        console.log('正在上传，请稍后！');
        layersObj.uploadProgress(1);
    },
    // 当文件添加到上传队列失败时触发此事件，失败的原因可能是文件大小超过了你允许的数值、文件是空的或者文件队列已经满员了等。
    fileQueueError: function(fileObj, error, msg) {
        console.log("文件添加到上传队列失败,  错误代码： " + error + "  错误信息： " + msg);
        if (error == -110) {
            console.error('上传失败，图片尺寸过大！图片不能大于2M');
        }
    },
    // 当文件选取完毕且选取的文件经过处理后（指添加到上传队列），会立即触发该事件
    fileDialogComplete: function(selected, queued, total) {
        if (upload1.fn2) {
            upload1.fn2();
            if (upload1.fn2()) {
                // 开始上传文件
                this.startUpload();
            }
        } else {
            // 开始上传文件
            this.startUpload();
        }
    },
    // 文件上传被中断或是文件没有成功上传时会触发该事件。停止、取消文件上传或是在uploadStart事件中返回false都会引发这个事件，但是如果某个文件被取消了但仍然还在队列中则不会触发该事件
    uploadError: function(fileObj, error, msg) {
        console.log("文件上传失败,  错误代码： " + error + "  错误信息： " + msg);
        if (error == -200) {
            console.error('上传失败，请刷新页面重试！');
        }
    },
    // 当一个文件上传成功后会触发该事件
    uploadSuccess: function(fileObj, serverData) {
        console.log('上传完成！');
        serverData = JSON.parse(serverData);
        if (upload1.fn1) {
            layer.close(uploadProgress);
            upload1.fn1(serverData.org_img_uri, fileObj);
        } else {
            // $('#' + this.movieName).siblings('.coverPic_img').attr({
            //     'src': serverData.org_img_uri,
            //     'data-url': 1
            // });
        }
        return fileObj;
    },
};
// 音频段落
var upload2 = {
    fn1: null,
    fn2: null,
    // 初始化
    init: function(obj) {
        var settings = {
            // 基本配置
            flash_url: "../../../../vv-base/js/swfupload/swfupload.swf",
            upload_url: "http://mp-up.51vv.com/upload_v_article",
            file_size_limit: "2 MB",
            file_types: obj.flieType,
            file_types_description: "All Files",
            file_upload_limit: 0,
            file_queue_limit: 1,
            debug: false,

            // Button settings
            button_image_url: obj.btnImg,
            button_width: obj.btnWidth,
            button_height: obj.btnHeight,
            button_placeholder_id: obj.btnId,
            button_cursor: SWFUpload.CURSOR.HAND,
            button_action: SWFUpload.BUTTON_ACTION.SELECT_FILES,

            // 定义上传完成的事件
            file_queued_handler: this.fileQueued,
            file_queue_error_handler: this.fileQueueError,
            file_dialog_complete_handler: this.fileDialogComplete,
            upload_success_handler: this.uploadSuccess,
            upload_error_handler: this.uploadError
        };
        var swfu = new SWFUpload(settings);
        this.fn1 = obj.fn1;
        this.fn2 = obj.fn2;
        return swfu;
    },
    // 当一个文件被添加到上传队列时会触发此事件，提供的唯一参数为包含该文件信息的file object对象
    fileQueued: function(fileObj) {
        console.log('正在上传，请稍后！');
        layersObj.uploadProgress(2);
    },
    // 当文件添加到上传队列失败时触发此事件，失败的原因可能是文件大小超过了你允许的数值、文件是空的或者文件队列已经满员了等。
    fileQueueError: function(fileObj, error, msg) {
        console.log("文件添加到上传队列失败,  错误代码： " + error + "  错误信息： " + msg);
        if (error == -110) {
            console.error('上传失败，图片尺寸过大！图片不能大于2M');
        }
    },
    // 当文件选取完毕且选取的文件经过处理后（指添加到上传队列），会立即触发该事件
    fileDialogComplete: function(selected, queued, total) {
        if (upload2.fn2) {
            upload2.fn2();
            if (upload2.fn2()) {
                // 开始上传文件
                this.startUpload();
            }
        } else {
            // 开始上传文件
            this.startUpload();
        }
    },
    // 文件上传被中断或是文件没有成功上传时会触发该事件。停止、取消文件上传或是在uploadStart事件中返回false都会引发这个事件，但是如果某个文件被取消了但仍然还在队列中则不会触发该事件
    uploadError: function(fileObj, error, msg) {
        console.log("文件上传失败,  错误代码： " + error + "  错误信息： " + msg);
        if (error == -200) {
            console.error('上传失败，请刷新页面重试！');
        }
    },
    // 当一个文件上传成功后会触发该事件
    uploadSuccess: function(fileObj, serverData) {
        console.log('上传完成！');
        serverData = JSON.parse(serverData);
        if (upload2.fn1) {
            layer.close(uploadProgress);
            upload2.fn1(serverData.org_img_uri, fileObj);
        } else {
            // $('#' + this.movieName).siblings('.coverPic_img').attr({
            //     'src': serverData.org_img_uri,
            //     'data-url': 1
            // });
        }
        return fileObj;
    },
};
// 视频段落
var upload3 = {
    fn1: null,
    fn2: null,
    // 初始化
    init: function(obj) {
        var settings = {
            // 基本配置
            flash_url: "../../../../vv-base/js/swfupload/swfupload.swf",
            upload_url: "http://mp-up.51vv.com/upload_v_article",
            file_size_limit: "2 MB",
            file_types: obj.flieType,
            file_types_description: "All Files",
            file_upload_limit: 0,
            file_queue_limit: 1,
            debug: false,

            // Button settings
            button_image_url: obj.btnImg,
            button_width: obj.btnWidth,
            button_height: obj.btnHeight,
            button_placeholder_id: obj.btnId,
            button_cursor: SWFUpload.CURSOR.HAND,
            button_action: SWFUpload.BUTTON_ACTION.SELECT_FILES,

            // 定义上传完成的事件
            file_queued_handler: this.fileQueued,
            file_queue_error_handler: this.fileQueueError,
            file_dialog_complete_handler: this.fileDialogComplete,
            upload_success_handler: this.uploadSuccess,
            upload_error_handler: this.uploadError
        };
        var swfu = new SWFUpload(settings);
        this.fn1 = obj.fn1;
        this.fn2 = obj.fn2;
        return swfu;
    },
    // 当一个文件被添加到上传队列时会触发此事件，提供的唯一参数为包含该文件信息的file object对象
    fileQueued: function(fileObj) {
        console.log('正在上传，请稍后！');
        layersObj.uploadProgress(3);
    },
    // 当文件添加到上传队列失败时触发此事件，失败的原因可能是文件大小超过了你允许的数值、文件是空的或者文件队列已经满员了等。
    fileQueueError: function(fileObj, error, msg) {
        console.log("文件添加到上传队列失败,  错误代码： " + error + "  错误信息： " + msg);
        if (error == -110) {
            console.error('上传失败，图片尺寸过大！图片不能大于2M');
        }
    },
    // 当文件选取完毕且选取的文件经过处理后（指添加到上传队列），会立即触发该事件
    fileDialogComplete: function(selected, queued, total) {
        if (upload3.fn2) {
            upload3.fn2();
            if (upload3.fn2()) {
                // 开始上传文件
                this.startUpload();
            }
        } else {
            // 开始上传文件
            this.startUpload();
        }
    },
    // 文件上传被中断或是文件没有成功上传时会触发该事件。停止、取消文件上传或是在uploadStart事件中返回false都会引发这个事件，但是如果某个文件被取消了但仍然还在队列中则不会触发该事件
    uploadError: function(fileObj, error, msg) {
        console.log("文件上传失败,  错误代码： " + error + "  错误信息： " + msg);
        if (error == -200) {
            console.error('上传失败，请刷新页面重试！');
        }
    },
    // 当一个文件上传成功后会触发该事件
    uploadSuccess: function(fileObj, serverData) {
        console.log('上传完成！');
        serverData = JSON.parse(serverData);
        if (upload3.fn1) {
            layer.close(uploadProgress);
            upload3.fn1(serverData.org_img_uri, fileObj);
        } else {
            // $('#' + this.movieName).siblings('.coverPic_img').attr({
            //     'src': serverData.org_img_uri,
            //     'data-url': 1
            // });
        }
        return fileObj;
    },
};
// 图片段落
var upload4 = {
    fn1: null,
    fn2: null,
    // 初始化
    init: function(obj) {
        var settings = {
            // 基本配置
            flash_url: "../../../../vv-base/js/swfupload/swfupload.swf",
            upload_url: "http://mp-up.51vv.com/upload_v_article",
            file_size_limit: "2 MB",
            file_types: obj.flieType,
            file_types_description: "All Files",
            file_upload_limit: 0,
            file_queue_limit: 1,
            debug: false,

            // Button settings
            button_image_url: obj.btnImg,
            button_width: obj.btnWidth,
            button_height: obj.btnHeight,
            button_placeholder_id: obj.btnId,
            button_cursor: SWFUpload.CURSOR.HAND,
            button_action: SWFUpload.BUTTON_ACTION.SELECT_FILES,

            // 定义上传完成的事件
            file_queued_handler: this.fileQueued,
            file_queue_error_handler: this.fileQueueError,
            file_dialog_complete_handler: this.fileDialogComplete,
            upload_success_handler: this.uploadSuccess,
            upload_error_handler: this.uploadError
        };
        var swfu = new SWFUpload(settings);
        this.fn1 = obj.fn1;
        this.fn2 = obj.fn2;
        return swfu;
    },
    // 当一个文件被添加到上传队列时会触发此事件，提供的唯一参数为包含该文件信息的file object对象
    fileQueued: function(fileObj) {
        console.log('正在上传，请稍后！');
        layersObj.uploadProgress(1);
    },
    // 当文件添加到上传队列失败时触发此事件，失败的原因可能是文件大小超过了你允许的数值、文件是空的或者文件队列已经满员了等。
    fileQueueError: function(fileObj, error, msg) {
        console.log("文件添加到上传队列失败,  错误代码： " + error + "  错误信息： " + msg);
        if (error == -110) {
            console.error('上传失败，图片尺寸过大！图片不能大于2M');
        }
    },
    // 当文件选取完毕且选取的文件经过处理后（指添加到上传队列），会立即触发该事件
    fileDialogComplete: function(selected, queued, total) {
        if (upload4.fn2) {
            upload4.fn2();
            if (upload4.fn2()) {
                // 开始上传文件
                this.startUpload();
            }
        } else {
            // 开始上传文件
            this.startUpload();
        }
    },
    // 文件上传被中断或是文件没有成功上传时会触发该事件。停止、取消文件上传或是在uploadStart事件中返回false都会引发这个事件，但是如果某个文件被取消了但仍然还在队列中则不会触发该事件
    uploadError: function(fileObj, error, msg) {
        console.log("文件上传失败,  错误代码： " + error + "  错误信息： " + msg);
        if (error == -200) {
            console.error('上传失败，请刷新页面重试！');
        }
    },
    // 当一个文件上传成功后会触发该事件
    uploadSuccess: function(fileObj, serverData) {
        console.log('上传完成！');
        serverData = JSON.parse(serverData);
        if (upload4.fn1) {
            layer.close(uploadProgress);
            upload4.fn1(serverData.org_img_uri, fileObj);
        } else {
            // $('#' + this.movieName).siblings('.coverPic_img').attr({
            //     'src': serverData.org_img_uri,
            //     'data-url': 1
            // });
        }
        return fileObj;
    },
};