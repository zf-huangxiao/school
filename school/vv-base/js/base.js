window.base = {
    //获取url参数
    getUrlParam : function(name) {
        var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)"); //构造一个含有目标参数的正则表达式对象
        var r = window.location.search.substr(1).match(reg);  //匹配目标参数
        if (r != null) {
            return r[2];
        } else {
            return null; //返回参数值
        }
    },
    // 格式化数量（最多显示到9999，超出显示1.2w，末位向下取整。1w不显示为1.0w）
    formatNum: function (num,radix) {
        return (num > 9999) ? parseInt((num/radix)*10)/10 + 'w' : num;
    }
};
