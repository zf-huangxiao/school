// 图片裁剪成width*height
var arr = ['upcdn','chuangshicdn'];
function pictureTrim (pictureUrl,width,height) {
    if (!pictureUrl) return;
    for (var i = 0; i < arr.length; i++) {
        if (pictureUrl.indexOf(arr[i]) != -1) {
            var pictureUrl = pictureUrl.replace(/\?\S*/ig,'');
            var suffix = pictureUrl.substring(pictureUrl.lastIndexOf(".") + 1, pictureUrl.length); //后缀名
            suffix = suffix == 'bmp' ? 'jpg' : suffix;
            var str;
            if (!width && !height) {
                str = '.cw750.';
            } else if (width && height) {
                str = '.cs' + width + 'x' + height + '.';
            } else if (width && !height) {
                str = '.cw' + width + '.';
            }
            return pictureUrl.replace('.cw750.'+ suffix ,'') + str + suffix;
        }
    }
    return pictureUrl;
}