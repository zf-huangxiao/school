 function versionCompare(number) {
     var ver = String(number).split('.');
     var ver_01 = ver[0] * 255 * 255 * 255;
     var ver_02 = ver[1] * 255 * 255;
     var ver_03 = ver[2] * 255;
     var ver_04 = ver[3];
     return parseFloat(ver_01 + ver_02 + ver_03 + ver_04);
 }

 function Version(curV, reqV) {
     var arr1 = curV.split('.');
     var arr2 = reqV.split('.');
     //将两个版本号拆成数字 
     var minL = Math.min(arr1.length, arr2.length);
     var pos = 0; //当前比较位
     var diff = 0; //当前为位比较是否相等
     //逐个比较如果当前位相等则继续比较下一位
     while (pos < minL) {
         diff = parseInt(arr1[pos]) - parseInt(arr2[pos]);
         if (diff != 0) {
             break;
         }
         pos++;
     }
     if (diff >= 0) {
         return true;
     } else {
         return false;
     }
 }