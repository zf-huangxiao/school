// 分享 复制链接 copyobject 隐藏的input标签 _shareUrl 分享的链接地址，sucobject 分享成功后的弹窗提示
function shareCopyLink(_shareUrl, sucobject) {
    if (!sucobject) {
        sucobject = $('.popup_copyLink');
    }
    var copyobject = document.getElementById("copy-content");
    copyobject.value = _shareUrl;
    copyobject.select();
    document.execCommand("Copy");
    // 复制链接成功 弹窗
    sucobject.show();
    setTimeout(function() {
        sucobject.hide();
    }, 2000);
}