/**
 * 格式化时间方法
 * @ date 时间参数，传入字符串
 * @ fmt 时间显示格式，‘yyyy-MM-dd’
 * suyue
 */
function formatDate(time, fmt) {
    if (time) {
        var date = new Date(time);
        var o = {
            'y+': date.getFullYear(),
            'M+': date.getMonth() + 1,
            'd+': date.getDate(),
            'h+': date.getHours(),
            'm+': date.getMinutes(),
            's+': date.getSeconds()
        };
        for (var k in o) {
            if (new RegExp('(' + k + ')').test(fmt)) {
                var str = o[k] + '';
                fmt = fmt.replace(RegExp.$1, (str.length === 1) ? ('0' + str) : str);
            }
        };
        return fmt;
    } else {
        return '';
    }
}