/**
 * [createCanvas 生成二维码 需要引入先qrcode.js]
 * @param  {[type]} options [
 *  var payCodePrm = {
        render      : "canvas",
        background      : "#ffffff",
        foreground      : "#000000",
        width: 154, //宽度 
        height: 154, //高度 
        typeNumber: -1,
        correctLevel: 0, //纠错等级  
        text: 'https://h5.51vv.com/cms/zb/index.htm'//二维码url地址
    }
 * ]
 * @return {[type]}         [description]
 * suyue
 */


function createCanvas(options) {
    // create the qrcode itself
    var qrcode = new QRCode(options.typeNumber, options.correctLevel);
    qrcode.addData(options.text);
    qrcode.make();

    // create canvas element
    var canvas = document.createElement('canvas');
    canvas.width = options.width;
    canvas.height = options.height;
    var ctx = canvas.getContext('2d');

    // compute tileW/tileH based on options.width/options.height
    var tileW = options.width / qrcode.getModuleCount();
    var tileH = options.height / qrcode.getModuleCount();

    // draw in the canvas
    for (var row = 0; row < qrcode.getModuleCount(); row++) {
        for (var col = 0; col < qrcode.getModuleCount(); col++) {
            ctx.fillStyle = qrcode.isDark(row, col) ? options.foreground : options.background;
            var w = (Math.ceil((col + 1) * tileW) - Math.floor(col * tileW));
            var h = (Math.ceil((row + 1) * tileW) - Math.floor(row * tileW));
            ctx.fillRect(Math.round(col * tileW), Math.round(row * tileH), w, h);
        }
    }
    // return just built canvas
    return canvas;
}