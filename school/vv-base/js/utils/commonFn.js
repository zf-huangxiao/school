/**
 * Created by hx on 2018/4/17.
 */
let commonFnObj = {
    //处理图片url（裁剪）
    handlePicUrl: function (url, pWidth, pHeight) {
        if (!url) return;
        url = url.replace('//file.m.mvbox.cn/', '//upcdn.file.m.mvbox.cn/');
        let typeStr = url.substring(url.lastIndexOf('.') + 1);
        typeStr = typeStr == 'bmp' ? 'jpg' : typeStr;
        if (!pWidth) return url;
        if (pWidth && !pHeight) {
            return url + '.cw' + pWidth + '.' + typeStr;
        } else if (pWidth && pHeight) {
            return url + '.cs' + pWidth + 'x' + pHeight + '.' + typeStr;
        }
    },
    //格式化如:1.5w
    formatNum: function (num) {
        if (num >= 10000) {
            return (num / 1000 | 0) / 10 + '万';
        }
        return num;
    },
    //进入歌房
    enterRoom: function (roomInfo) {
        var obj = {
            type: 'enterKroomFromHall',
            roomInfo: roomInfo,
        };
        //console.log(JSON.stringify(obj));
        window.QCefClient.invokeMethod('client', JSON.stringify(obj));
    },
    //进入直播间
    enterLive: function (liveID) {
        window.QCefClient.invokeMethod('client', '{"type": "activityOpenLive", "liveID": "' + liveID + '"}');
    },
    //进入个人主页
    enterHomePage: function (uid) {
        window.QCefClient.invokeMethod('client', '{"type": "openHomePage", "userID": ' + uid + '}');
    },
    //关注或取消关注 type1关注 2取消关注
    followOrNot: function (toUserID,userID,type,callback) {
        const parameter = {
            toUserID:toUserID,
            userID:userID,
            type:type
        };
        var followOrNotPms = {
            type: "https://music.51vv.com/sod?action=29",
            parameter:  '&parameter=' + JSON.stringify(parameter),
            base: location.href,
            callback: "followOrNotCB"
        };
        window.followOrNotCB = callback;
        window.QCefClient.invokeMethod('server', JSON.stringify(followOrNotPms));
    },
    //创建我的直播
    buildLiveRoom: function (callback) {
        console.log('buildLiveRoom');
        var obj = {
            type: 'launchKroomCreate',
            base: location.href,
            callback: 'buildLiveRoomCallback'
        };
        window.buildLiveRoomCallback = callback || function () {
                console.log('build success')
            };
        window.QCefClient.invokeMethod('client', JSON.stringify(obj));
    },
    //创建我的歌房
    buildSingRoom: function (callback) {
        console.log('buildSingRoom');
        var obj = {
            type: 'launchLiveCreate',
            base: location.href,
            callback: 'buildSingRoomCallback'
        };
        window.buildSingRoomCallback = callback || function () {
                console.log('build success')
            };
        window.QCefClient.invokeMethod('client', JSON.stringify(obj));
    },
    //获取父级元素(getParentsEle(ele,'div-.name1-.name2'))
    getParentsEle: function (ele, selector) {
        let parentEle = ele.parentNode;
        const selectorArr = selector.trim().split('-');
        const checkEle = ele => {
            const eleClassArr = ele.className.trim().split(' ');
            let resultBol = true;
            for (let i = 0; i < selectorArr.length; i++) {
                if (selectorArr[i].toLowerCase()[0] === '.') {
                    //class
                    if (eleClassArr.indexOf(selectorArr[i].replace('.', '')) === -1) {
                        resultBol = false;
                        break;
                    }

                } else {
                    //element
                    if (ele.tagName.toLowerCase() !== selectorArr[i]) {
                        resultBol = false;
                        break;
                    }
                }
            }
            return resultBol;
        };
        while (parentEle && parentEle.nodeType === 1) {
            if (checkEle(parentEle)) {
                return parentEle;
            } else {
                parentEle = parentEle.parentNode;
            }
        }
    },

    //端外ajax请求数据
    ajaxGet: function (url, params, success) {
        // console.log(this)
        this.$http.get(url, {
            params: params
        }).then(res => res.json()).then(success.bind(this));
    },
    //端内走协议请求数据
    clientGet: function (url, params, success) {
        let parameter;
        if (url.includes('?')) {
            parameter = '';
        } else {
            parameter = '?';
        }
        for (let key in params) {
            parameter += '&' + key + '=' + params[key];
        }
        if (parameter.includes('?')) {
            parameter = parameter.replace('?&', '?')
        }
        let getData = {
            type: url,
            parameter: parameter,
            rtnReqParams:1,
            params:params,
            base: location.href,
            callback: 'showData'
        };
        window.showData = success.bind(this);
        window.QCefClient.invokeMethod('server', JSON.stringify(getData));
    },
    getUrlParam: function (name, url) {
        if (!url) url = location.href;
        name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
        var regexS = "[\\?&]" + name + "=([^&#]*)";
        var regex = new RegExp(regexS);
        var results = regex.exec(url);
        return results == null ? null : results[1];
    }
};
module.exports = commonFnObj;
