/**
 * 评论
 * 前提：
 * 1.引用时放在页面创建评论框的JS部分之前（如果要使用commentHandle中的comBoxStr来创建评论框部分HTML）
 * 2.要在页面中localStorage存phoneNumState(手机验证的状态)
 */

/*var loginUserId = window.loginUserId;*/
var initComment = (function () {
    var commentHandle, webcommentHandle;
    //端内评论列表
    commentHandle = {
        currentUserId: common.getUrlParam("userId") || common.getUrlParam("loginUserId"),
        contentId: window.contentId,
        userId: window.dynameicUserId,
        viewNumber: 15,
        createTime: '',
        comCreateTime: 0, // 加载评论使用的动态createtime、选择器、动态id
        comSelector: '',
        comContentId: '',
        favourFlag: 'true',
        commentFlag: 'true',
        dynamicScroll: 'true',
        isClick: true,
        netState: true,
        isCanDelete: true,
        timer: null,
        comBoxStr: '<div class="comment-wrap" id="commentWrap" name="commentWrap">' +
        '<div class="triangle-comment-more1"></div>' +
        '<div class="triangle-comment-more2"></div>' +
        '<div class="comment_con clearfix"> ' +
        '<div class="comment_input_box"> ' +
        '<div type="text" class="comment_input" contenteditable="true"></div> ' +
        '<span class="emoji-wrap emoji-wrap-btn gray-emoji" data-type="1"></span> ' +
        '</div> ' +
        '<span class="comment_btn">评论</span> ' +
        '<div class="dynamic-emoji"> ' +
        '<div class="box-ext"> ' +
        '<div id="emoji-box" class="emoji-box open"> ' +
        '<div id="item-box" class="item-box"></div> ' +
        '<p id="emoji-tip" class="emoji-tip"></p> ' +
        '</div> ' +
        '</div> ' +
        '</div> ' +
        '</div> ' +
        '<!-- 评论列表 --> <div class="comment_list"> ' +
        '</div> ' +
        '<div class="reveive-dynamic-emoji" > ' +
        '<div class="box-ext"> ' +
        '<div id="emoji-box" class="emoji-box open"> ' +
        '<div id="item-box" class="item-box"></div> ' +
        '<p id="emoji-tip" class="emoji-tip"></p> ' +
        '</div> ' +
        '</div> ' +
        '</div> ' +
        '<div class="comment-load-more">' +
        '<img class="comment-load-img" src="images/friends/loading.gif">' +
        '<span class="comment-load-span">加载更多</span>' +
        '</div>' +
        '<div class="comment-load-arrows">' +
        '<span class="comment-arrows-span comment-arrows-txt"></span>' +
        '<span class="comment-arrows-span comment-arrows-span-color comment-down">展开更多评论</span>' +
        '<span class="comment-arrows-img comment-arrows-img1"></span>' +
        '</div>' +
        '<div class="comment-load-try">' +
        '<span class="comment-arrows-span comment-arrows-txt">加载失败，</span>' +
        '<span class="comment-arrows-span comment-arrows-span-color comment-try-again">请重试</span>' +
        '</div>' +
        '</div>' +
        '</div>' +
        '</div>',
        init: function (obj) {
            this.initListBtn = obj.initListBtn;//点击按钮弹出评论列表
            this.comContainer = obj.comContainer;//评论列表外层盒子
            this.listsContainer = obj.listsContainer;//内容列表外部容器（如动态列表容器）
            this.comNumSel = obj.comNumSel;//评论条数所在元素
            this.loadMoreType = obj.loadMoreType || 'click';//'click'点击按钮加载更多,'scroll'，滚动加载更多
            this.disHide = obj.disHide,//true禁止切换评论的显示隐藏
                commentHandle.eventBind(obj);
        },
        // 评论
        commentList: function (data) {
            // console.info(currentUserId);
            var str = '',
                img, nickNameStr = '',
                receiveNameStr, parentComment;
            data.forEach(function (v, k) {
                img = v.commentUserSimpleInfo.userImg || 'images/friends/avatar_icon.png';
                userNameStr = common.formatStr(v.commentUserSimpleInfo.nickName);
                parentComment = (v.parentCommentDelState == 1) ? '该评论已删除' : commentHandle.readEmoji(v.parentComment);
                str += '<div class="comment-details" data-commentUserId="' + v.commentUserId + '">' +
                    '<div class="comment-info clearfix">' +
                    '<div class="comment-avatar-wrap">' +
                    '<img class="comment-avatar" src="' + img + '">' +
                    (function () {
                        if (v.commentUserSimpleInfo.vip && v.commentUserSimpleInfo.vip.length > 0) {
                            return '<span class="comment-auth zoom_btn" style="background-image:url(' + common.getSmallVipLevel(v.commentUserSimpleInfo.vip[0]) + ')"></span>';
                        } else if (v.commentUserSimpleInfo.vipImgUrl) {
                            return '<span class="comment-auth zoom_btn" style="background-image:url(' + v.commentUserSimpleInfo.vipImgUrl + ')"></span>';
                        } else if (v.commentUserSimpleInfo.levelImgUrl) {
                            return '<span class="comment-auth zoom_btn" style="background-image:url(' + v.commentUserSimpleInfo.levelImgUrl + ')"></span>';
                        } else {
                            return '';
                        }
                    })() +
                    '</div>' +
                    '<div class="comment-user-details">' +
                    '<div class="toggle-wrap clearfix">' +
                    (function () {
                        if (v.commentUserSimpleInfo.vip && v.commentUserSimpleInfo.vip.length > 0) {
                            return '<span class="comment-nick comment-nick-color ellipsis" title="' + userNameStr + '">' + userNameStr + '：</span>';
                        } else {
                            return '<span class="comment-nick ellipsis" title="' + userNameStr + '">' + userNameStr + '：</span>';
                        }
                    })() +
                    '<span class="comment-txt comment-txt-height">' + commentHandle.readEmoji(v.comment) + '</span>' +
                    '<div class="comment-toggle-txt"></div>' +
                    '</div>' +
                    (function () {
                        var str = '';
                        if (v.receiveCommentUserId) {
                            receiveNameStr = common.formatStr(v.receiveCommentUserSimpleInfo.nickName);
                            str += '<div class="receive-comment-txt">' +
                                '<div class="receive-toggle-wrap">' +
                                '<span class="receive-txt-height">' +
                                (function () {
                                    if (v.parentCommentDelState == 1) {
                                        return "该评论已删除";
                                    } else {
                                        return '<span class="comment-nick revceive-com-nick ellipsis" title="' + receiveNameStr + '">@' + receiveNameStr + '</span>' + '<span class="comment-nick">：</span>' + parentComment;
                                    }
                                })() +
                                '</span>' +
                                '<div class="comment-toggle-receive"></div>' +
                                '</div>' +
                                '</div>';
                        }
                        return str;
                    })() +
                    '<span class="comment-times">' + commentHandle.formatTime(v.createTime) +
                    '<span class="receive-btn">回复</span>' +
                    (function () {
                        return (commentHandle.currentUserId == v.commentUserId) ? '<span class="comment-delete" data-delcontentid="' + v.contentId + '" data-delcommentid="' + v.id + '">删除</span>' : '';
                    })() +
                    '</span>' +
                    '</div>' +
                    '</div>' +
                    '<div class="receive_con clearfix" data-commentUserId="' + v.commentUserId + '" data-contentid="' + v.contentId + '"data-parentid="' + v.id + '">' +
                    '<div class="receive_input_box">' +
                    '<div type="text" class="receive_input" contenteditable="true" placeholder="说点什么吧...."></div>' +
                    '<input class="emoji-wrap comment-emoji-btn comment-emoji gray-emoji" data-type="2" type="button" />' +
                    '</div>' +
                    '<span class="receive_btn receive_btn_disabled">评论</span>' +
                    '<div class="dynamic-emoji">' +
                    '<div class="box-ext"><div id="emoji-box" class="emoji-box open"><div id="item-box" class="item-box"></div><p id="emoji-tip" class="emoji-tip"></p></div></div>' +
                    '</div>' +
                    '</div>' +
                    '</div>';
            });
            return str;
        },

        // 格式化时间
        formatTime: function (time) {
            var newTime = +new Date() / 1000;
            var creatTime = parseInt(time, 10) / 1000;
            var str = '';
            // 发布时间
            var year = new Date(time).getFullYear();
            var month = new Date(time).getMonth() + 1;
            var date = new Date(time).getDate();
            var hours = new Date(time).getHours();
            var minutes = new Date(time).getMinutes();
            var seconds = new Date(time).getSeconds();
            // 现在时间
            var nowYear = new Date().getFullYear();
            var nowMonth = new Date().getMonth() + 1;
            var nowDate = new Date().getDate();
            var nowHours = new Date().getHours();
            var nowMinutes = new Date().getMinutes();
            var nowSeconds = new Date().getSeconds();

            // 本月最后一天
            function getMonthLastDay(year, month) {
                month = parseInt(month, 10);
                var d = new Date(year, month, 0);
                return d.getDate();
            }

            var lastDay = parseInt(getMonthLastDay(year, month), 10);

            // 补全零位
            function addZero(n) {
                var n = String(n);
                n = (n < 10) ? '0' + n : n;
                return n.slice(-2);
            }

            var disTime = newTime - creatTime;
            if (month == nowMonth) {
                if (disTime <= 60) {
                    str = '刚刚';
                } else if (disTime > 60 && disTime <= 60 * 60) {
                    str = parseInt(disTime / 60, 10) + '分钟前';
                } else if (disTime > 60 * 60 && disTime <= 3600 * 24) {
                    str = parseInt(disTime / 3600, 10) + '小时前';
                } else if (disTime > 3600 * 24 && disTime <= 3600 * 24 * lastDay) {
                    str = (nowDate - date) + '天前';
                } else {
                    str = year + '年' + addZero(month) + '月' + addZero(date) + '日';
                }
            } else {
                var lastMonthDays = parseInt(getMonthLastDay(year, month), 10);
                str = (lastMonthDays + nowDate - date) + '天前';
            }
            return str
        },


        // 读取表情
        renderEmoji: function () {
            var emoji_path = '//music.51vv.com/pc/mp_content/dist/images/emoji/';
            var emoji = window._emoji.list;
            if (emoji) {
                var counter = 0;
                var msg = '<table><tbdy>';
                msg += '<tr>';
                for (var em in emoji) {
                    msg += '<td data-tip="' + emoji[em].tip + '"><span class="td-span"><img class="emoji" src="' + emoji_path + emoji[em].png + '" data-id="' + emoji[em].id + '" data-gif="' + emoji_path + emoji[em].gif + '" data-png="' + emoji_path + emoji[em].png + '" data-tip="' + emoji[em].tip + '"/></span></td>'
                    counter++;
                    if (counter % 9 == 0) {
                        msg += '</tr><tr>';
                    }
                }
                msg += '</tr>';
                msg += '</tbdy></table>';
                return msg;
            }
        },

        // 转换表情
        readEmoji: function (msg) {
            var emojiMsg = msg;
            // var emojiArr = window._emoji.list;
            var emojiPath = '//music.51vv.com/pc/mp_content/dist/images/emoji/';
            // var emojiPath = '../images/emoji/';
            if (emojiMsg) {
                emojiMsg = emojiMsg.replace(/(1\.[02]\.\d{1,3}\.)(?:png|gif)/ig, '<img class="emoji-img" src="' + emojiPath + '$1png"/>');
            }
            return emojiMsg;
        },

        // 上报次数
        reportHandle: function (key, para) {
            var resInfoPms = JSON.stringify(para);
            window.QCefClient.invokeMethod('client', '{"type":"reportStat","key":"' + key + '","value":' + resInfoPms + '}');
        },


        // 拉取评论列表
        getCommentList: function (selector, comContainer, contentId, type) {
            selector = selector instanceof $ ? selector : $(selector);
            if (selector && commentHandle.loadMoreType == 'click') {//点击“评论”按钮加载列表,点击加载更多来加载更多
                var _this = this;
                commentHandle.comSelector = selector ? selector : commentHandle.comSelector;
                commentHandle.comContentId = contentId ? contentId : commentHandle.comContentId;
                commentHandle.comCreateTime = type == 'first' ? '0' : commentHandle.comCreateTime;
                var num = parseInt(selector.html(), 10);
                // 评论参数
                var commentObj = {
                    type: 'https://music.51vv.com/user/comment/getCommentList.htm',
                    parameter: '?contentId=' + commentHandle.comContentId + '&viewNumber=5&createTime=' + commentHandle.comCreateTime,
                    base: location.href,
                    callback: 'commentListCallBack'

                };
                commentHandle.comSelector.parents("[data-contentid]").parent().find('.comment-load-try').hide();
                //评论回调
                window.commentListCallBack = function (res) {
                    var listBox = commentHandle.comSelector.parents("[data-contentid]"),
                        listsBox = listBox.parent();

                    listBox.find('.comment_input')[0].focus();
                    if (res.result == 0) {
                        var list, arrGrep, createTime;
                        _this.isClick = true;
                        list = res.userComments || [];
                        listBox.find(comContainer).show();
                        listsBox.find('.comment-load-more').hide();
                        if (list.length > 0) {
                            commentHandle.dynamicScroll = 'true';
                            if (type == 'first') {
                                listBox.find('.comment_list').html(commentHandle.commentList(list));
                                if (list.length >= 5 && num > 5) {
                                    listsBox.find('.comment-load-arrows').show().find('.comment-arrows-txt').html('');
                                    listsBox.find('.comment-load-arrows').show().find('.comment-arrows-span-color').removeClass('comment-up').addClass('comment-down').html('展开更多评论');
                                    listsBox.find('.comment-arrows-img').show().removeClass('comment-arrows-img2').addClass('comment-arrows-img1');
                                } else {
                                    listsBox.find('.comment-load-arrows').hide();
                                }
                            } else {
                                listBox.find('.comment_list').append(commentHandle.commentList(list));
                                if (list.length < 5) {
                                    listsBox.find('.comment-load-arrows').show().find('.comment-arrows-txt').html('已经展开全部评论,');
                                    listsBox.find('.comment-load-arrows').show().find('.comment-arrows-span-color').removeClass('comment-down').addClass('comment-up').html('向上收起');
                                    listsBox.find('.comment-arrows-img').show().removeClass('comment-arrows-img1').addClass('comment-arrows-img2');
                                } else {
                                    listsBox.find('.comment-load-arrows').show().find('.comment-arrows-txt').html('');
                                    listsBox.find('.comment-load-arrows').show().find('.comment-arrows-span-color').removeClass('comment-up').addClass('comment-down').html('展开更多评论');
                                    listsBox.find('.comment-arrows-img').show().removeClass('comment-arrows-img2').addClass('comment-arrows-img1');
                                }
                            }
                            listsBox.find('.comment-txt-height').each(function (k, v) {
                                if (v.offsetHeight > 72) {
                                    listsBox.find('.comment-toggle-txt').eq(k).attr('data-height', v.offsetHeight + 20);
                                    listsBox.find('.comment-toggle-txt').eq(k).attr('data-type', '1');
                                    listsBox.find('.comment-toggle-txt').eq(k).html('...&nbsp;&nbsp;<span class="toggle-span-txt">展开</span>').show();
                                }
                            });
                            listsBox.find('.receive-txt-height').each(function (k, v) {
                                if (v.offsetHeight > 72) {
                                    listsBox.find('.comment-toggle-receive').eq(k).attr('data-height', v.offsetHeight + 20);
                                    listsBox.find('.comment-toggle-receive').eq(k).attr('data-type', '1');
                                    listsBox.find('.comment-toggle-receive').eq(k).html('...&nbsp;&nbsp;<span class="toggle-span-txt-receive">展开</span>').show();
                                }
                            });
                            commentHandle.comCreateTime = list[list.length - 1].createTime;
                        } else {
                            if (type == 'first') {
                                listsBox.find('.comment-load-arrows').hide();
                            } else {
                                listsBox.find('.comment-load-arrows').show().find('.comment-arrows-txt').html('已经展开全部评论,');
                                listsBox.find('.comment-load-arrows').show().find('.comment-arrows-span-color').removeClass('comment-down').addClass('comment-up').html('向上收起');
                                listsBox.find('.comment-arrows-img').show().removeClass('comment-arrows-img1').addClass('comment-arrows-img2');
                            }
                        }
                    } else {
                        if (type == 'first') {
                            commentHandle.comSelector.siblings('.comment-more').children('.comment_loading').hide();
                            commentHandle.comSelector.siblings('.comment-more').children('.comment-fail').show();
                        } else {
                            listsBox.find('.comment-load-arrows').show().find('.comment-arrows-txt').html('加载失败，');
                            listsBox.find('.comment-load-arrows').show().find('.comment-arrows-span-color').removeClass('comment-up').removeClass('comment-down').html('请重试');
                            listsBox.find('.comment-arrows-img').hide();
                        }
                    }
                    // 滚动加载
                    listsBox.on('click', '.comment-down,.comment-arrows-img1,.comment-try-again', function () {
                        if (_this.isClick) {
                            listsBox.find('.comment-load-more').show();
                            listsBox.find('.comment-load-arrows').hide();
                            _this.isClick = false;
                            commentHandle.netState = navigator.onLine;
                            if (!commentHandle.netState) {
                                listsBox.find('.comment-load-more').hide();
                                listsBox.find('.comment-load-arrows').hide();
                                listsBox.find('.comment-load-try').show();
                                return;
                            }
                            commentHandle.getCommentList(commentHandle.comSelector, comContainer, commentHandle.comContentId, 'more');
                        }
                    });
                    listsBox.on('click', '.comment-up,.comment-arrows-img2', function () {
                        commentHandle.netState = navigator.onLine;
                        if (!commentHandle.netState) {
                            listsBox.find('.comment-load-more').hide();
                            listsBox.find('.comment-load-arrows').hide();
                            listsBox.find('.comment-load-try').show();
                            return;
                        }
                        commentHandle.getCommentList(commentHandle.comSelector, comContainer, commentHandle.comContentId, 'first');
                    });
                }

            } else if (selector && commentHandle.loadMoreType == 'scroll') {//滚动加载列表
                commentHandle.comContentId = contentId ? contentId : commentHandle.comContentId;
                commentHandle.comCreateTime = type == 'first' ? '0' : commentHandle.comCreateTime;
                // 评论参数
                var commentObj = {
                    type: 'https://music.51vv.com/user/comment/getCommentList.htm',
                    parameter: '?contentId=' + commentHandle.comContentId + '&viewNumber=' + commentHandle.viewNumber + '&createTime=' + commentHandle.comCreateTime,
                    base: location.href,
                    callback: 'commentListCallBack'
                };
                //评论回调
                window.commentListCallBack = function (res) {
                    if (res.result == 0) {
                        var list, arrGrep, createTime;
                        list = res.userComments || [];
                        if (list.length > 0) {
                            commentHandle.dynamicScroll = 'true';
                            if ($(comContainer).css('display', 'none')) $(comContainer).show();
                            if (type == 'first') {
                                $(comContainer).find('.comment_list').html(commentHandle.commentList(list));
                            } else {
                                $(comContainer).find('.comment_list').append(commentHandle.commentList(list));
                            }
                            if (list.length < commentHandle.viewNumber) {
                                $(comContainer).find('.dynamic-list-loading').find('.dynamic-list-load-img').hide();
                                $(comContainer).find('.dynamic-list-loading').find('.dynamic-list-load-tip').html('没有更多了').hide();
                            } else {
                                $(comContainer).find('.dynamic-list-loading').find('.dynamic-list-load-img').show();
                                $(comContainer).find('.dynamic-list-loading').find('.dynamic-list-load-tip').html('加载中');
                            }
                            $(commentHandle.listsContainer).find('.dynamic-list-loading').show();
                            $(commentHandle.listsContainer).find('.comment-txt-height').each(function (k, v) {
                                if (v.offsetHeight > 72) {
                                    $(commentHandle.listsContainer).find('.comment-toggle-txt').eq(k).attr('data-height', v.offsetHeight + 20);
                                    $(commentHandle.listsContainer).find('.comment-toggle-txt').eq(k).attr('data-type', '1');
                                    $(commentHandle.listsContainer).find('.comment-toggle-txt').eq(k).html('...&nbsp;&nbsp;<span class="toggle-span-txt">展开</span>').show();
                                }
                            });
                            $(commentHandle.listsContainer).find('.receive-txt-height').each(function (k, v) {
                                if (v.offsetHeight > 72) {
                                    $(commentHandle.listsContainer).find('.comment-toggle-receive').eq(k).attr('data-height', v.offsetHeight + 20);
                                    $(commentHandle.listsContainer).find('.comment-toggle-receive').eq(k).attr('data-type', '1');
                                    $(commentHandle.listsContainer).find('.comment-toggle-receive').eq(k).html('...&nbsp;&nbsp;<span class="toggle-span-txt-receive">展开</span>').show();
                                }
                            });
                            commentHandle.comCreateTime = list[list.length - 1].createTime;
                        } else {
                            if (type == 'first' && list.length == 0) {
                                $(commentHandle.listsContainer).find('.dynamic-list-loading').hide();
                            } else {
                                $(commentHandle.listsContainer).find('.dynamic-list-loading').find('.dynamic-list-load-img').hide();
                                $(commentHandle.listsContainer).find('.dynamic-list-loading').find('.dynamic-list-load-tip').html('没有更多了').hide();
                                $(commentHandle.listsContainer).find('.dynamic-list-loading').show();
                            }
                        }
                    }
                };
            }

            //请求评论

            if (typeof QCefClient != 'undefined') {
                window.QCefClient.invokeMethod('server', JSON.stringify(commentObj));
            }
        },

        // 发表评论
        comment: function (para) {
            commentHandle.comCreateTime = '';
            var message = para.comment;
            //替换表情信息
            message = message.replace(/<img.*?data-id="(.*?)" data-sign="vv-emoji"\/?>/ig, '$1');
            //替换 空格，大于号，小于号，连字符
            message = message.replace(/&nbsp;/g, ' ').replace(/&lt;/g, '<').replace(/&gt;/g, '>').replace(/&amp;/g, '&');
            //替换信息中的 < & 空格
            message = message.replace(/</g, '<\u00ad');
            message = encodeURIComponent(message);
            // 评论参数
            if (para.type == '0') {
                var commentObj = {
                    type: 'https://music.51vv.com/user/comment/publish.htm',
                    parameter: '?contentId=' + para.contentId + '&comment=' + message + '&receiveCommentUserId=' + para.receiveCommentUserId,
                    base: location.href,
                    callback: 'commentCallBack'
                };
            } else {
                var commentObj = {
                    type: 'https://music.51vv.com/user/comment/publish.htm',
                    parameter: '?contentId=' + para.contentId + '&comment=' + message + '&receiveCommentUserId=' + para.receiveCommentUserId + '&parentCommentId=' + para.parentId,
                    base: location.href,
                    callback: 'commentCallBack'
                };
            }

            //评论回调
            window.commentCallBack = function (res) {
                if (res.result == 0) {

                    if (res.userComment.parentCommentDelState == 1) {
                        $('.comment_del_popup').show();
                        commentHandle.commentFlag = 'true';
                        setTimeout(function () {
                            $('.comment_del_popup').hide();
                        }, 1000);
                    } else {
                        var numHtml = para.selector.html() == '评论' ? 0 : parseInt(para.selector.html(), 10);
                        numHtml++;
                        para.selector.html(numHtml);
                        // $('.comment_btn  span').html(numHtml);
                        $('#commentPopup').removeClass('comment_fail').addClass('comment_success').html('评论成功').show();
                        commentHandle.getCommentList(para.selector, commentHandle.comContainer, commentHandle.comContentId, 'first');

                        para.$el.html('');
                        $('.default-txt').show();
                        commentHandle.commentFlag = 'true';


                        if (numHtml <= 0) {
                            $('.fixed_comment').addClass('num_empty')
                        } else {
                            $('.fixed_comment').removeClass('num_empty')
                        }
                    }
                    commentHandle.setFocus($('.comment_input'));
                } else if (res.result == 10067) {  //手机验证
                    commentHandle.phoneVer();
                    commentHandle.commentFlag = 'true';
                } else {
                    $('#commentPopup').removeClass('comment_success').addClass('comment_fail').html('评论失败').show();
                    commentHandle.commentFlag = 'true';
                }
                setTimeout(function () {
                    $('#commentPopup').hide();
                }, 1000);
                para.$el.siblings('.emoji-wrap').removeClass('lighter-emoji').addClass('gray-emoji');
            };
            //请求评论
            window.QCefClient.invokeMethod('server', JSON.stringify(commentObj));
        },

        // 删除评论
        delComment: function (contentId, commentId, $el, $elwrap) {
            var _this = this;
            var deleteObj = {
                type: 'https://music.51vv.com/user/comment/del.htm',
                parameter: '?contentId=' + contentId + '&commentId=' + commentId,
                base: location.href,
                callback: 'deleteObjCallback'
            };
            // 删除回调函数
            window.deleteObjCallback = function (res) {

                if (res.result == 0) {
                    $el.remove();
                    var numHtml = parseInt($elwrap.html(), 10);
                    numHtml--;
                    if (numHtml == 0) {
                        numHtml = '评论';
                    }
                    if (numHtml <= 5) {
                        $(commentHandle.listsContainer).find('.comment-load-arrows').hide();
                    }

                    $elwrap.html(numHtml);
                    $('#delCommentPopup').removeClass('del-comment_fail').addClass('del-comment_success').html('删除成功').show();
                    if (numHtml <= 0) {
                        $('.fixed_comment').addClass('num_empty')
                    } else {
                        $('.fixed_comment').removeClass('num_empty')
                    }
                } else {
                    $('#delCommentPopup').removeClass('del-comment_success').addClass('del-comment_fail').html('删除失败').show();
                }
                setTimeout(function () {
                    $('#delCommentPopup').hide();
                }, 1000);
            };
            window.QCefClient.invokeMethod('server', JSON.stringify(deleteObj));
        },


        // 跳转个人中心
        jumpHome: function (userID) {
            window.QCefClient.invokeMethod('client', '{"type":"openHomePage","userID":"' + userID + '"}');
        },
        // 设置光标
        setFocus: function ($el) {
            el = $el[0];
            el.focus();
            if ($.support.msie) {
                var range = document.selection.createRange();
                this.last = range;
                range.moveToElementText(el);
                range.select();
                document.selection.empty(); //取消选中
            } else {
                var range = document.createRange();
                range.selectNodeContents(el);
                range.collapse(false);
                var sel = window.getSelection();
                sel.removeAllRanges();
                sel.addRange(range);
            }
        },

        // 未读信息
        unReadMsg: function () {
            // 未读参数
            var unReadPms = {
                type: 'https://music.51vv.com/user/content/getUnReadMsg.htm',
                parameter: '?viewNumber=' + commentHandle.viewNumber + '&createTime' + commentHandle.createTime,
                base: location.href,
                callback: 'unReadCallBack'
            };
            //未读回调
            window.unReadCallBack = function (res) {
                if (res.result == 0) {

                } else {

                }
            };
            //请求未读
            window.QCefClient.invokeMethod('server', JSON.stringify(unReadPms));
        },

        // 事件绑定
        eventBind: function (obj) {
            // 展开评论列表
            if (obj.initListBtn && obj.loadMoreType == 'click') {
                var thisCommentBtn, thisContentId, thisCommetNum;
                $(obj.listsContainer).on('click', obj.initListBtn, function () {
                    thisCommentBtn = $(this);
                    thisCommetNum = $(this).html();
                    thisContentId = $(this).parents('[data-contentid]').attr('data-contentid');
                    if ($(this).parents('[data-contentid]').children(obj.comContainer).css('display') == 'none') {
                        $(obj.listsContainer).find(obj.comContainer).hide();
                        commentHandle.getCommentList($(this), obj.comContainer, thisContentId, 'first');
                        $('.comment-more').hide();
                        $('.dynamicList').find('.dynamic-list-loading').show();
                        if (thisCommetNum != '评论') {
                            $(this).siblings('.comment-more').show();
                        }
                        $(this).parents('[data-contentid]').find('.comment_input').trigger('input');
                        $(this).parents('[data-contentid]').find('.comment_input')[0].focus();

                    } else {
                        $('.comment-more').hide();
                        $(this).parents('[data-contentid]').children(obj.comContainer).hide();
                        $(obj.listsContainer).find('.dynamic-list-loading').hide();
                    }
                    $(obj.listsContainer).find('.dynamic-emoji').hide();
                    $(obj.listsContainer).find('.comment_con').find('.comment_input').html('');
                    // handle.setFocus($(this).parents('.dynamic-outer').find('.comment_input'));
                    // $(this).parents('[data-contentid]').find('.comment_input')[0].focus();
                    var elFocus = $(this).parents('[data-contentid]').find('.comment_input')[0];
                    setTimeout(function () {
                        elFocus.focus();
                    }, 300)

                });
            } else if (obj.initListBtn && obj.loadMoreType == 'scroll') {
                if (obj.disHide) {
                    thisCommetNum = $(obj.initListBtn).html();
                    thisContentId = $(obj.initListBtn).parents('[data-contentid]').attr('data-contentid');
                    commentHandle.dynamicScroll = 'false';
                    commentHandle.getCommentList($(obj.initListBtn), obj.comContainer, thisContentId, 'first');
                    $(obj.listsContainer).find(obj.comContainer).show();
                    $('.comment-more').hide();
                    $('.dynamicList').find('.dynamic-list-loading').show();
                    if (thisCommetNum != '评论') {
                        $(this).siblings('.comment-more').show();
                    }
                    $(obj.initListBtn).parents('[data-contentid]').find('.comment_input').trigger('input');
                    $(obj.initListBtn).parents('[data-contentid]').find('.comment_input')[0].focus();
                    $(obj.listsContainer).find('.dynamic-emoji').hide();
                    $(obj.listsContainer).find('.comment_con').find('.comment_input').html('');
                    var elFocus = $(obj.initListBtn).parents('[data-contentid]').find('.comment_input')[0];
                    setTimeout(function () {
                        elFocus.focus();
                    }, 300)
                } else {
                    $(obj.listsContainer).on('click', obj.initListBtn, function () {
                        thisCommentBtn = $(this);
                        thisCommetNum = $(this).html();
                        thisContentId = $(this).parents('[data-contentid]').attr('data-contentid');
                        if ($(this).parents('[data-contentid]').children(obj.comContainer).css('display') == 'none') {
                            commentHandle.dynamicScroll = 'true';
                            commentHandle.getCommentList($(this), obj.comContainer, thisContentId, 'first');
                            $(obj.listsContainer).find(obj.comContainer).show();
                            $('.comment-more').hide();
                            $('.dynamicList').find('.dynamic-list-loading').show();
                            if (thisCommetNum != '评论') {
                                $(this).siblings('.comment-more').show();
                            }
                            $(this).parents('[data-contentid]').find('.comment_input').trigger('input');
                            $(this).parents('[data-contentid]').find('.comment_input')[0].focus();
                        } else {
                            commentHandle.dynamicScroll = 'false';
                            $('.comment-more').hide();
                            $(this).parents('[data-contentid]').children(obj.comContainer).hide();
                            $(obj.listsContainer).find('.dynamic-list-loading').hide();
                        }
                        $(obj.listsContainer).find('.dynamic-emoji').hide();
                        $(obj.listsContainer).find('.comment_con').find('.comment_input').html('');
                        // $(this).parents('[data-contentid]').find('.comment_input')[0].focus();
                        var elFocus = $(this).parents('[data-contentid]').find('.comment_input')[0];
                        setTimeout(function () {
                            elFocus.focus();
                        }, 300)

                    });

                }

            }
            // 评论列表滚动
            if (obj.initListBtn && obj.loadMoreType == 'scroll') {
                var getElTimer = setInterval(function () {
                    if ($(obj.initListBtn).parents('[data-contentid]').length) {
                        clearInterval(getElTimer);
                        var id = $(obj.initListBtn).parents('[data-contentid]').attr('data-contentid');
                        if ($(obj.comContainer).css('display') == 'block' && !obj.disHide) {
                            $(obj.initListBtn).click();
                            $(obj.initListBtn).click();
                        } else if ($(obj.comContainer).css('display') == 'none' && !obj.disHide) {
                            $(obj.initListBtn).click();
                        }
                    }
                }, 700);

                $(window).on('scroll', function (event) {
                    var clientHeight = $('body')[0].clientHeight;
                    var scrollTop = document.documentElement.scrollTop || document.body.scrollTop;
                    var height = $(window).height();
                    var scrollNetState = navigator.onLine;
                    if (height + scrollTop > clientHeight - 200 && commentHandle.dynamicScroll == 'true') {
                        commentHandle.dynamicScroll = 'false';
                        if (scrollNetState) {
                            commentHandle.getCommentList($(obj.initListBtn), commentHandle.comContainer, commentHandle.comContentId, 'more');
                        } else {
                            $('.dynamic-no-more').html('加载失败');
                            setTimeout(function () {
                                commentHandle.dynamicScroll = 'true';
                            }, 800);
                        }
                    }
                });
            }
            // 评论输入框状态
            var commentTxt,
                comOuterBox;
            comOuterBox = $(obj.listsContainer);
            comOuterBox.on('change', '.comment_input', function () {
                commentTxt = $(this).html();
                if (commentTxt.replace(/\s/g, '')) { //判断输入的是否为空或者为空格
                    $(this).siblings('.emoji-wrap').addClass('lighter-emoji').removeClass('gray-emoji');
                    $('.comment_input_box').addClass('comment_input_box_border');
                    $('.receive_btn').removeClass('receive_btn_disabled');
                    $('.default-txt').show();
                    $(this).siblings('.default-txt').hide();
                } else {
                    $(this).siblings('.emoji-wrap').addClass('gray-emoji').removeClass('lighter-emoji');
                    $('.comment_input_box').removeClass('comment_input_box_border');
                    $('.receive_btn').addClass('receive_btn_disabled');
                    $('.default-txt').show();
                }
            });
            comOuterBox.on('input', '.comment_input', function () {
                commentTxt = $(this).html();
                var strReplace = $(this)[0].innerText.length + $(this)[0].childElementCount;
                if (strReplace > 150) {
                    $('.comment-text-limit').show();
                    setTimeout(function () {
                        $('.comment-text-limit').hide();
                    }, 1000);
                    return;
                }
            });
            comOuterBox.on('focus', '.comment_input', function () {
                $('.default-txt').show();
                $(this).siblings('.default-txt').hide();
                $('.comment_input_box').addClass('comment_input_box_border');
                // 手机验证提示
                commentHandle.phoneTip($(this))
            });
            comOuterBox.on('blur', '.comment_input', function () {
                if (!$(this).html()) {
                    $('.default-txt').show();
                    $('.comment_input_box').removeClass('comment_input_box_border');
                }
            });


            // 评论重新加载
            $(obj.listsContainer).on('click', '#reloadBtn', function (event) {
                event.preventDefault();
                if (commentHandle.comNumSel) {//点击获取
                    $(this).parents('[data-contentid]').find(commentHandle.comNumSel).click();
                } else {//自动获取
                    commentHandle.getCommentList('', commentHandle.comContainer, contentId, 'first');
                }
            });

            // 发布评论
            comOuterBox.on('click', '.comment_btn', function () {
                if (!navigator.onLine) {
                    $('.comment-text-fail').show();
                    setTimeout(function () {
                        $('.comment-text-fail').hide();
                    }, 1000);
                }
                var para = {
                    type: '0',
                    selector: $(this).parents('[data-contentid]').find(obj.comNumSel),
                    contentId: $(this).parents('[data-contentid]').attr('data-contentid'),
                    comment: $(this).siblings('.comment_input_box').children('.comment_input').html(),
                    receiveCommentUserId: $(this).parents('[data-userid]').attr('data-userid'),
                    $el: $(this).siblings('.comment_input_box').find('.comment_input')
                }
                var strReplace = para.comment.replace(/<img.*?(?:>|\/>)/gi, '1');
                if (strReplace.length > 150) {

                    $('.comment-text-limit').show();
                    setTimeout(function () {
                        $('.comment-text-limit').hide();
                    }, 1000);
                } else if (strReplace != '' && strReplace.length <= 150) {
                    $(this).addClass('comment_btn_click');
                    $('.comment_input_box').addClass('comment_input_box_border');
                    $(this).siblings('.comment_input_box').find('.emoji-wrap').removeClass('gray-emoji').addClass('lighter-emoji');
                    if (commentHandle.commentFlag == 'true') {
                        commentHandle.commentFlag = 'false';
                        commentHandle.comment(para);

                    }
                } else {
                    $('.common-text-no').show();
                    setTimeout(function () {
                        $('.common-text-no').hide();
                    }, 1000);
                }
                $(this).removeClass('comment_btn_click');
            });
            //删除评论
            var delContentId, delCommentId;
            $(obj.listsContainer).on('click', '.comment-delete', function () {
                var $el = $(this).parents('.comment-details');
                var $elwrap = $(this).parents('[data-contentid]').find(obj.comNumSel);
                delContentId = $(this).data('delcontentid');
                delCommentId = $(this).data('delcommentid');
                commentHandle.delComment(delContentId, delCommentId, $el, $elwrap);
            });

            // 一级回复时相关状态检测
            $(obj.listsContainer).on('change', '.receive_input', function () {
                if ($(this).html().replace(/\s/g, '')) { //判断输入的是否为空或者为空格
                    $(this).siblings('.emoji-wrap').addClass('lighter-emoji').removeClass('gray-emoji');
                    $('.receive_btn').removeClass('receive_btn_disabled');
                    $('.receive_input_box').addClass('receiv_input_box_border');
                    $('.receive-default-txt').show();
                    $(this).siblings('.receive-default-txt').hide();
                } else {
                    $(this).siblings('.emoji-wrap').addClass('gray-emoji').removeClass('lighter-emoji');
                    $('.receive_btn').addClass('receive_btn_disabled');
                    $('.receive_input_box').removeClass('receiv_input_box_border');
                    $('.default-txt').show();
                }
            });
            $(obj.listsContainer).on('focus', '.receive_input', function () {
                $('.receive-default-txt').show();
                $(this).siblings('.receive-default-txt').hide();
                $('.receive_input_box').addClass('receiv_input_box_border');
                // 手机验证提示
                commentHandle.phoneTip($(this))

            });
            $(obj.listsContainer).on('blur', '.receive_input', function () {
                if (!$(this).html()) {
                    $('.receive-default-txt').show();
                    $('.receive_input_box').removeClass('receiv_input_box_border');
                }
            });
            $(obj.listsContainer).on('input', '.receive_input', function () {
                commrntTxt = $(this).html();
                var strReplace = $(this)[0].innerText.length + $(this)[0].childElementCount;
                if (strReplace > 150) {
                    $('.comment-text-limit').show();
                    setTimeout(function () {
                        $('.comment-text-limit').hide();
                    }, 1000);
                    return;
                }
            });

            // 二级回复相关状态
            $(obj.listsContainer).on('keyup', '.next_receive_input', function () {
                if ($(this).html().replace(/\s/g, '')) { //判断输入的是否为空或者为空格
                    $(this).siblings('.emoji-wrap').addClass('lighter-emoji').removeClass('gray-emoji');
                    $('.comment_input_box').addClass('comment_input_box_border');
                    $('.next_receive_btn').removeClass('next_receive_btn_disabled');
                } else {
                    $(this).siblings('.emoji-wrap').addClass('gray-emoji').removeClass('lighter-emoji');
                    $('.comment_input_box').removeClass('comment_input_box_border');
                    $('.next_receive_btn').addClass('next_receive_btn_disabled');
                }
            });

            //粘贴处理
            $(obj.listsContainer).on('paste', '.comment_input', function (e) {
                var e = e || window.event;
                var clipboar = e.clipboardData || e.originalEvent.clipboardData;
                if (clipboar) {
                    var text = clipboar.getData('Text');
                    var number = $(this)[0].innerText.length + $(this)[0].childElementCount;
                    $(this).insertAtCaret(text);
                    e.preventDefault();
                }
            });
            $(obj.listsContainer).on('paste', '.receive_input', function (e) {
                var e = e || window.event;
                var clipboar = e.clipboardData || e.originalEvent.clipboardData;
                if (clipboar) {
                    var text = clipboar.getData('Text');
                    var number = $(this)[0].innerText.length + $(this)[0].childElementCount;
                    $(this).insertAtCaret(text);
                    e.preventDefault();
                }
            });
            // 回复(点击处理相关状态)
            $(obj.listsContainer).on('click', '.receive-btn', function () {
                if (!navigator.onLine) {
                    $('.comment-text-fail').show();
                    setTimeout(function () {
                        $('.comment-text-fail').hide();
                    }, 1000);
                }
                $(obj.listsContainer).find('.next_receive_con').css('display', 'none');
                $('.receive-default-txt').show();
                var position = $(this).offset().top;
                var scroll = $('body').scrollTop();
                var height = $(window).height();
                if ($(this).parents('.comment-info').siblings('.receive_con').css('display') == 'none') {
                    $(obj.listsContainer).find('.receive_con').hide();
                    if (scroll + height - position < 60) {
                        $(this).parents('.comment-info').siblings('.receive_con').show();
                        $('body').scrollTop(scroll + 60);
                    } else {
                        $(this).parents('.comment-info').siblings('.receive_con').show();
                    }
                } else {
                    $(obj.listsContainer).find('.receive_con').hide();
                    $(this).parents('.comment-info').siblings('.receive_con').hide();
                }
                $(obj.listsContainer).find('.dynamic-emoji').hide();
                $(obj.listsContainer).find('.receive_con').find('.receive_input').html('');
                $(this).parents('.comment-info').siblings('.receive_con').find('.receive_input')[0].focus();
            });

            // 发布回复
            $(obj.listsContainer).on('click', '.receive_btn', function () {
                commentHandle.reportHandle('dynamicReceive', {
                    'contentId': true
                });
                // 回复type:1
                var para = {
                    type: '1',
                    selector: $(this).parents('[data-contentid]').find(commentHandle.comNumSel),
                    contentId: $(this).parents('[data-contentid]').data('contentid'),
                    comment: $(this).siblings('.receive_input_box').find('.receive_input').html(),
                    parentId: $(this).parents('.receive_con').data('parentid'),
                    receiveCommentUserId: $(this).parents('.receive_con').data('commentuserid'),
                    $el: $(this).siblings('.receive_input_box').find('.receive_input')
                }
                var strReplace = para.comment.replace(/<img.*?(?:>|\/>)/gi, '1');
                if (strReplace.length > 150) {
                    $('.comment-text-limit').show();
                    setTimeout(function () {
                        $('.comment-text-limit').hide();
                    }, 1000);
                } else if (strReplace != '' && strReplace.length <= 150) {
                    $(this).addClass('receive_btn_click');
                    $('.receive_input_box').addClass('receive_input_box_border');
                    $(this).siblings('.receive_input_box').find('.emoji-wrap').removeClass('gray-emoji').addClass('lighter-emoji');
                    if (commentHandle.commentFlag == 'true') {
                        commentHandle.commentFlag = 'false';
                        commentHandle.comment(para);
                    }
                } else {
                    $('.common-text-no').show();
                    setTimeout(function () {
                        $('.common-text-no').hide();
                    }, 1000);
                }
            });

            // 跳转个人中心
            $(obj.listsContainer).on('click', '.avatar-wrap, .nick', function () {
                commentHandle.jumpHome($(this).parents('[data-userid]').attr('data-userid'));
            });
            $(obj.listsContainer).on('click', '.comment-avatar-wrap, .comment-nick', function () {
                commentHandle.jumpHome($(this).parents('.comment-details').attr('data-commentUserId'));
            });
            $(obj.listsContainer).on('click', '.dynamic-at', function () {
                commentHandle.jumpHome($(this).data('atuser'));
            });

            // 评论表情
            $(obj.listsContainer).on('click', '.emoji-wrap-btn', function (event) {
                var type = $(this).data('type');
                var isHidden = $(this).parents('.comment_con').find('.dynamic-emoji').is(":hidden");
                event.stopPropagation();
                $(this).parents('.comment_con').find('.dynamic-emoji').find('.item-box').html(commentHandle.renderEmoji());
                $(obj.listsContainer).find('.comment_con').find('.dynamic-emoji').hide();
                $(obj.listsContainer).find('.dynamic-emoji').hide();

                if (isHidden) {
                    $(this).parents('.comment_con').find('.dynamic-emoji').show();
                } else {
                    $(this).parents('.comment_con').find('.dynamic-emoji').hide();
                }
                var $emoji_box = $(this).parents('.comment_con').find('.dynamic-emoji').find('.item-box');
                var $elInput = $(this).siblings('.comment_input');
                emojiHandl($emoji_box, $elInput);
                $(obj.listsContainer).find('.reveive-dynamic-emoji').hide();
            });

            // 一级回复评论表情
            $(obj.listsContainer).on('click', '.comment-emoji-btn', function (event) {
                var type = $(this).data('type');
                var isHidden = $(this).parents('.receive_con').find('.dynamic-emoji').is(":hidden");
                event.stopPropagation();
                $(this).parents('.receive_con').find('.dynamic-emoji').find('.item-box').html(commentHandle.renderEmoji());
                $(obj.listsContainer).find('.reveive-dynamic-emoji').hide();
                $(obj.listsContainer).find('.dynamic-emoji').hide();
                if (isHidden) {
                    $(this).parents('.receive_con').find('.dynamic-emoji').show();
                } else {
                    $(this).parents('.receive_con').find('.dynamic-emoji').hide();
                }
                var $emoji_box = $(this).parents(obj.comContainer).find('.reveive-dynamic-emoji').find('.item-box');
                var $elInput = $(this).siblings('.receive_input');
                emojiHandl($emoji_box, $elInput);
                $(obj.listsContainer).find('.comment_con').find('.dynamic-emoji').hide();
            });

            function emojiHandl($emoji_box, $elInput, type) {
                //表情事件绑定
                $('td').hover(function () {
                    $(this).find('.td-span').addClass('border-span');
                    var _tip_top = 160;
                    if (this.dataset.tip) {
                        var _left = this.offsetLeft > 230 ? '230' : this.offsetLeft + 30;
                        var _top = this.offsetTop - $emoji_box.scrollTop() > _tip_top ? (_tip_top + 5) : this.offsetTop - $emoji_box.scrollTop() + 35;
                        $('#emoji-tip').text(this.dataset.tip).css({
                            'left': _left + 'px',
                            'top': _top + 'px'
                        }).show();
                    }
                }, function () {
                    $(this).find('.td-span').removeClass('border-span');
                    $('.emoji-tip').hide();
                }).click(function (e) {
                    //插入表情
                    $('.content').find('.comment_con').find('.dynamic-emoji').hide();
                    $('.content').find('.receive_con').find('.dynamic-emoji').hide();
                    $('.content').find('.next_receive_con').find('.dynamic-emoji').hide();
                    if (e.target.nodeName == 'IMG') selectEmoji(this, $elInput);
                    commentHandle.setFocus($elInput);
                });
                //插入表情
                function selectEmoji(ele, $elInput) {

                    var strReplace = $elInput[0].innerText.length + $elInput[0].childElementCount;
                    if (strReplace > 150) {
                        $('.comment-text-limit').show();
                        setTimeout(function () {
                            $('.comment-text-limit').hide();
                        }, 1000);
                        return;
                    }
                    var img = $(ele).find('img')[0];
                    var str = '<img src="' + img.dataset.png + '" data-id="' + img.dataset.id + '" data-sign="vv-emoji"/>';
                    $elInput.insertAtCaret(str);
                }

                //向可编辑div里插入内容
                $.fn.extend({
                    insertAtCaret: function (val) {
                        //光标位置  光标在表情前面--上一个文本的结尾   光标在表情后面--输入框子节点的序号
                        var selection = window.getSelection && window.getSelection();
                        var anchorNode = selection.anchorNode;
                        var anchorOffset = selection.anchorOffset;
                        //输入框
                        var pn = this[0];
                        //选区
                        var ran = selection.getRangeAt(0);
                        //插入的表情
                        var imgNode = $.parseHTML(val)[0];
                        if (anchorNode.nodeType == 3) {
                            //文本中插入
                            if (selection.anchorOffset == anchorNode.nodeValue.length) {
                                //在文本结尾
                                pn.insertBefore(imgNode, anchorNode.nextElementSibling);
                            } else if (selection.anchorOffset == 0) {
                                //在文本开头
                                pn.insertBefore(imgNode, anchorNode);
                            } else {
                                //在文本中 拆分后半部分文本并插入
                                var n2 = document.createTextNode(anchorNode.nodeValue.substring(selection.anchorOffset));
                                pn.replaceChild(n2, anchorNode);
                                //在后半部分文本前插入表情
                                imgNode = pn.insertBefore(imgNode, n2);
                                //插入前半部分文本
                                if (anchorOffset) {
                                    var n1 = document.createTextNode(anchorNode.nodeValue.substring(0, anchorOffset));
                                    pn.insertBefore(n1, imgNode);
                                }
                            }
                        } else if (anchorNode.nodeType == 1) {
                            //输入框子节点插入
                            //在选中节点的后面一个节点，在它之前插入
                            pn.insertBefore(imgNode, pn.childNodes[anchorOffset]);
                        }
                        //结尾表情被隐藏处理176
                        if (imgNode.offsetLeft - pn.scrollLeft > 176) {
                            pn.scrollLeft = imgNode.offsetLeft - 176;
                        }
                        //设置焦点
                        ran = ran.cloneRange();
                        ran.setStartAfter(imgNode);
                        selection.removeAllRanges();
                        selection.addRange(ran);
                        pn.focus();
                    }
                })
            }

            // 表情框隐藏
            $('body').on('click', function () {
                $('.dynamic-emoji').hide();
            });
            // 单条评论展开
            var comToggleHeight, comtToggleType;
            $(obj.listsContainer).on('click', '.comment-toggle-txt', function () {
                comToggleHeight = $(this).attr('data-height') + 'px';
                comtToggleType = $(this).attr('data-type');

                if (comtToggleType == 1) {
                    $(this).attr('data-type', '2');
                    $(this).parent().css('maxHeight', comToggleHeight);
                    $(this).html('<span class="toggle-span-txt">收起</span>');
                } else {
                    $(this).attr('data-type', '1');
                    $(this).parent().css('maxHeight', 72 + 'px');
                    $(this).html('...&nbsp;&nbsp;&nbsp;&nbsp;<span class="toggle-span-txt">展开</span>');
                }
            });

            // 二级单条评论展开
            var reComToggleHeight, reComtToggleType;
            $(obj.listsContainer).on('click', '.comment-toggle-receive', function () {
                reComToggleHeight = $(this).attr('data-height') + 'px';
                reComtToggleType = $(this).attr('data-type');
                if (reComtToggleType == 1) {
                    $(this).attr('data-type', '2');
                    $(this).parent().css('maxHeight', reComToggleHeight);
                    $(this).html('<span class="toggle-span-txt-receive">收起</span>');
                } else {
                    $(this).attr('data-type', '1');
                    $(this).parent().css('maxHeight', 66 + 'px');
                    $(this).html('...&nbsp;&nbsp;&nbsp;&nbsp;<span class="toggle-span-txt-receive">展开</span>');
                }
            });

            //向可编辑div里插入内容
            $.fn.extend({
                insertAtCaret: function (val) {
                    //光标位置  光标在表情前面--上一个文本的结尾   光标在表情后面--输入框子节点的序号
                    var selection = window.getSelection && window.getSelection();
                    var anchorNode = selection.anchorNode;
                    var anchorOffset = selection.anchorOffset;

                    //输入框
                    var pn = this[0];
                    //选区
                    var ran = selection.getRangeAt(0);
                    //插入的表情
                    var imgNode = $.parseHTML(val)[0];
                    if (anchorNode.nodeType == 3) {
                        //文本中插入
                        if (selection.anchorOffset == anchorNode.nodeValue.length) {
                            //在文本结尾
                            pn.insertBefore(imgNode, anchorNode.nextElementSibling);
                        } else if (selection.anchorOffset == 0) {
                            //在文本开头
                            pn.insertBefore(imgNode, anchorNode);
                        } else {
                            //在文本中 拆分后半部分文本并插入
                            var n2 = document.createTextNode(anchorNode.nodeValue.substring(selection.anchorOffset));
                            pn.replaceChild(n2, anchorNode);
                            //在后半部分文本前插入表情
                            imgNode = pn.insertBefore(imgNode, n2);
                            //插入前半部分文本
                            if (anchorOffset) {
                                var n1 = document.createTextNode(anchorNode.nodeValue.substring(0, anchorOffset));
                                pn.insertBefore(n1, imgNode);
                            }
                        }
                    } else if (anchorNode.nodeType == 1) {
                        //输入框子节点插入
                        //在选中节点的后面一个节点，在它之前插入
                        pn.insertBefore(imgNode, pn.childNodes[anchorOffset]);
                    }
                    //结尾表情被隐藏处理176
                    if (imgNode.offsetLeft - pn.scrollLeft > 176) {
                        pn.scrollLeft = imgNode.offsetLeft - 176;
                    }
                    //设置焦点
                    ran = ran.cloneRange();
                    ran.setStartAfter(imgNode);
                    selection.removeAllRanges();
                    selection.addRange(ran);
                    pn.focus();
                }
            });
            // 屏蔽回车
            document.onkeydown = function () {
                if (window.event && window.event.keyCode == 13) {
                    window.event.returnValue = false;
                }
            };


            // 屏蔽a标签
            $(obj.listsContainer).on('click', '.comment-wrap a', function (e) {
                event.preventDefault();
                event.stopPropagation();
                return false;
            });
        },
        //手机验证提示
        phoneTip: function ($el) {
            if (localStorage.getItem('phoneNumState') == '0') {
                $('.phone_detail').css({
                    'display': 'block',
                    'top': ($el.offset().top - 160) + 'px'
                });
                if (commentHandle.timer) {
                    window.clearTimeout(commentHandle.timer);
                }
                commentHandle.timer = window.setTimeout(function () {
                    $('.phone_detail').css({
                        'display': 'none',
                    });
                }, 5000)
            }
        },
        //手机验证
        phoneVer: function () {
            if (localStorage.getItem('phoneNumState') == '0') {
                window.QCefClient.invokeMethod('client', '{"type": "OpenAuthPhone", "from": "1"}');
                if (commentHandle.timer) {
                    window.clearTimeout(commentHandle.timer);
                }
                $('.phone_detail').css('display', 'none');
            }
        }

    };

    // 端外评论列表
    webcommentHandle = {
        contentId: window.contentId,
        viewNumber: 15,
        comCreateTime: '', // 加载评论使用的动态createtime、选择器、动态id
        dynamicScroll: 'false',
        init: function () {
            webcommentHandle.getCommentList('first');
            webcommentHandle.eventBind();
        },
        getCommentList: function (type) {
            $.ajax({
                type: 'GET',
                url: '//music.51vv.com/wx/s/getCommentList',
                data: {
                    contentId: webcommentHandle.contentId,
                    viewNumber: webcommentHandle.viewNumber,
                    createTime: webcommentHandle.comCreateTime
                },
                dataType: 'json',
                success: function (data) {
                    var list = data.userComments;
                    if (list && list.length > 0) {
                        webcommentHandle.dynamicScroll = 'true';
                        if (type == 'first') {
                            $(obj.listsContainer).find('.comment_list').html(commentHandle.commentList(list));
                        } else {
                            $(obj.listsContainer).find('.comment_list').append(commentHandle.commentList(list));
                        }
                        webcommentHandle.comCreateTime = list[list.length - 1].createTime;
                    }
                }
            })
        },
        //事件绑定
        eventBind: function () {
            //下载弹框
            $(obj.listsContainer).on('click', '.comment_btn,.receive-btn,.avatar-wrap, .nick,.comment-avatar-wrap, .comment-nick,.dynamic-at,.report_btn', function () {
                $('#downloadPopup').show();
            });
            $(obj.listsContainer).on('focus', '.comment_input', function () {
                $('#downloadPopup').show();
            });

            // 评论列表滚动
            $(window).on('scroll', function (event) {
                var clientHeight = $('body')[0].clientHeight;
                var scrollTop = document.documentElement.scrollTop || document.body.scrollTop;
                var height = $(window).height();
                var scrollNetState = navigator.onLine;
                if (height + scrollTop > clientHeight - 200 && webcommentHandle.dynamicScroll == 'true') {
                    webcommentHandle.dynamicScroll = 'false';
                    if (scrollNetState) {
                        webcommentHandle.getCommentList();
                    } else {
                        $('.dynamic-no-more').html('加载失败');
                        setTimeout(function () {
                            webcommentHandle.dynamicScroll = 'true';
                        }, 800)
                    }
                }
            });
        }
    };
    function init(obj) {
        var cssStr = '<link rel="stylesheet" type="text/css" href="https://music.51vv.com/pc/vv-base/css/comment.css"/>';
        $('head').find('link:last').after(cssStr);
        if (typeof QCefClient != 'undefined') {
            commentHandle.init(obj);
        } else {
            webcommentHandle.init(obj);
        }
    }

    return {
        init: init,
        comBoxStr:commentHandle.comBoxStr
    }
})();


/*
 obj示例
 obj = {
 initListBtn:'',//点击按钮弹出评论列表
 comContainer:'',//评论列表外层盒子
 listsContainer:'',//内容列表外部容器（如动态列表容器）
 comNumSel:'',//评论条数所在元素
 loadMoreType:'',//'click'点击按钮加载更多,'scroll'，滚动加载更多
 };
 */
