// 图片裁剪固定尺寸
function pictureCs(pictureUrl, width, height) {
    if (!pictureUrl) return;
    if (pictureUrl.indexOf('upcdn.mpres.51vv.com') == -1) return pictureUrl;
    var pictureUrl = pictureUrl.replace(/\?\S*/ig,'');
    var suffix = pictureUrl.substring(pictureUrl.lastIndexOf(".") + 1, pictureUrl.length); //后缀名
    suffix = suffix == 'bmp' ? 'jpg' : suffix;
    var str;
    if (!width && !height) {
        str = '.cw750.';
    } else if (width && height) {
        str = '.cs' + width + 'x' + height + '.';
    } else if (width && !height) {
        str = '.cw' + width + '.';
    }
    var picturePic750 = pictureUrl.indexOf(str) == -1 ? pictureUrl + str + suffix : pictureUrl;
    return picturePic750;
};