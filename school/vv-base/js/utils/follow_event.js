/*关注和直播事件*/
vv.event = {
	follow: function(){
		$('.ajax-load').on('click', '.follow[data-followtype="0"]', function() {
			var self = $(this);
			if (typeof QCefClient == 'undefined') {
		        location.href = 'https://www.51vv.cc/wx/pc/down/index.html';
		    } else {
			    var obj = {
			        type: "focus",
			        userID: self.attr('data-userid'),
			        base: location.href,
			        callback: "followCallback"
			    };
			    followCallback = function(data) {
			        if (data.result == 0) {
			        	self.attr('data-followtype', 1);
						self.removeClass('to_followed').addClass('followed').html('已关注');
			        } else if (data.result == 18) {
			            alert('关注失败');
			        }
			    };
			    window.QCefClient.invokeMethod('client', JSON.stringify(obj));
		    }
		});
	},
	live: function(){
		$('.ajax-load').on('click', '.live, .E-live', function() {
			var self = $(this);
			if (typeof QCefClient == 'undefined') {
		        location.href = 'https://www.51vv.cc/wx/pc/down/index.html';
		    } else {
			    var liveID = self.parents('li').find('.live').attr('data-liveid');
		        var userID = self.parents('li').find('.live').attr('data-userid');
		        if (base.getUrlParam('anchorID') || liveID == 0) {
                    window.QCefClient.invokeMethod('client', '{"type": "openUserInfo", "userID": "' + userID + '", "from": "chatBox"}');
		        } else {
                    window.QCefClient.invokeMethod('client', '{"type": "activityOpenLive", "liveID": "' + liveID + '"}');
		        }
		    }
		});
	},
	init: function(){
		var _this = this;
		_this.follow();
		_this.live();
	}
};
