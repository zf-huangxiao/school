/*延时加载数据*/ 
var clearDir = null;
vv.ajax = {
	cache: true,
	callback: function(obj, data) {
		if($(obj).attr("data-callback") && typeof vv.callback != "undefined"){
			var this_ = $(obj), method = this_.attr("data-callback");
			vv.callback[method] && vv.callback[method](data);
		}
	},
	ajax_load: function(obj, temID) {
		var url = $(obj).attr('data-url'),
			cacheTime = parseInt($(obj).attr("data-cacheTime")) || 0,
			_this = this;
		$(obj).attr('loaded', 'loaded');
		var NowTime = parseInt(new Date().getTime()/1000);
		var storageKey = $(obj).attr("storageKey") || url;
		if(storageSupport && _this.cache && cacheTime > 0){
			if(vv.storage.get(storageKey, 'content')){
				if(NowTime - vv.storage.get(storageKey, 'dataLine') >= cacheTime){
					_this.loadAjaxData(obj, url, NowTime, temID);
				}
				else{
					$(obj).css('background', 'none');
					var oData = vv.storage.get(storageKey, 'content');
					oData = oData.content ? oData.content : oData;
					var tmpl = $("#" + temID).html();
					var doTtmpl = doT.template(tmpl);
					$(obj).append(doTtmpl(oData));
					if($(obj).find("img").hasClass("lazyload")){
						var getMe = $(obj).find(".lazyload");
						_this.imgLazyLoad(getMe);
					}
					_this.callback(obj, oData);
					_this.setLiveAndFollow(obj, oData);
				}
			}
			else{
				_this.loadAjaxData(obj, url, NowTime, temID);
			}
		}
		else{
			_this.loadAjaxData(obj, url, NowTime, temID);
		}		
	},
	img_load: function(obj) {
		var _this = this;
		if($(obj).attr('loaded') == 'loaded') return;
		if($(obj).find("img").hasClass("lazyload")){
			var getMe=$(obj).find(".lazyload");
			_this.imgLazyLoad(getMe);
			$(obj).attr('loaded', 'loaded');	
		}
	},
	imgLazyLoad: function(obj){
		obj.each(function(){
			var oSrc=$(this).attr("_src"),
				me=$(this);
			me.attr("src",oSrc);
		})
	},
	getParam: function(name, str) {
        var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
        var r = str.match(reg);
        if(r != null) return r[2];
        else return null;
    },
    getTodayDate: function(){
		var now = new Date();
		now = new Date(now.getTime() - 3*60*60*1000);
		now = [now.getFullYear(), (now.getMonth() + 1) > 9 ?  (now.getMonth() + 1) : ('0' + (now.getMonth() + 1)), (now.getDate()) > 9 ? now.getDate() : ("0" + now.getDate())];
		return now.join('');
	},
	loadAjaxData: function(obj, url, NowTime, temID) {
		var args = $(obj).attr('data-page'),
			_this = this,
			json = { time: Math.random() };
		if($(obj).attr('data-user')) {
			json.userId = base.getUrlParam('userid') || '';
		}
		if($(obj).attr('data-anchor')) {
			json.anchorID = base.getUrlParam('anchorID') || '';
		}
		if($(obj).attr('data-createTime')) {
            if($(obj).find('li').last().size()) json.createTime = $(obj).find('li').last().attr('data-createTime') || '0';
            if($(obj).find('li').last().size()) json.weekId = $(obj).find('li').last().attr('data-weekId') || '0';
		}
		if(args) {
			_this.loadAjaxDataPage(obj, url, temID, args);
		} else {
			var storageKey = $(obj).attr("storageKey") || url;
			$.ajax({
				url: url,
	            data: json,
				success: function(data){
					if(data && data.result == 0) {
						var oData = data.content?data.content:data;
						var tmpl = $("#"+temID).html();
						var doTtmpl = doT.template(tmpl);
						$(obj).append(doTtmpl(oData));
						$(obj).css('background', 'none');
						if(storageSupport) {					
							vv.storage.set(storageKey, 'content', data);
							vv.storage.set(storageKey, 'dataLine', NowTime);										
						}
						_this.callback(obj, data);
						_this.setLiveAndFollow(obj, data);
					}
				},
				complete:function(){
					if($(obj).find("img").hasClass("lazyload")) {
						var getMe=$(obj).find(".lazyload");
						_this.imgLazyLoad(getMe);	
					}	
				}
			});
		}
	},
	loadAjaxDataPage: function(obj, url, temID, args) {
		var dataInfo = [],
			_this = this;
		var page = {
			index: 0,
			current: 1,
			web: _this.getParam('websize', args), // 每页显示条数
			server: _this.getParam('serversize', args) // 服务端返回条数
		};
		page.size = Math.ceil(page.server/page.web); // 假分页频率
		getAjax();
		$(obj).on('click', '.more', function() {
			$(obj).find('.loading, .loaded, .more').detach();
			$(obj).append('<span class="loading">加载中...</span>');
			if(page.index / page.size == page.current - 1) {
				getAjax();
			} else {
				getData();
			}
		});
		function getAjax() {
			var args = {
                curPage: page.current,
				viewNumber: page.server,
				time: Math.random()
			}
			if($(obj).attr('data-today')) {
                args.reportDay = _this.getTodayDate()
			}
			if($(obj).attr('data-user')) {
				args.userId = base.getUrlParam('userid') || '';
			}
			if($(obj).attr('data-anchor')) {
				args.anchorID = base.getUrlParam('anchorID') || '';
			}
			if($(obj).attr('data-createTime')) {
                if($(obj).find('li').last().size())  args.createTime = $(obj).find('li').last().attr('data-createTime') || '0';
                if($(obj).find('li').last().size())  args.weekId = $(obj).find('li').last().attr('data-weekId') || '0';
			}
			$.ajax({
				url: url,
	            data: args,
				success: function(data){
					if(data && data.result == 0) {
						dataInfo = dataInfo.concat(data.info.reportList ? data.info.reportList : data.info);
						$(obj).css('background', 'none');
						if (!dataInfo.length) {
							var empty = $(obj).attr('data-empty') || '暂无数据';
							$(obj).append('<span class="loaded">' + empty + '</span>');
							return;
						}
						getData();
						page.current++;
					}
				}
			});
		};
		function getData() {
			$(obj).find('.loading, .loaded, .more').detach();
			var oData = dataInfo.slice(page.index * page.web, (page.index + 1) * page.web);
			var oData = { info: oData, index: page.index };
			var tmpl = $("#"+temID).html();
			var doTtmpl = doT.template(tmpl);
			$(obj).append(doTtmpl(oData));
			page.index++;
			if(oData.info.length == page.web) {
				$(obj).append('<a href="javascript:;" class="more">加载更多 >></a>');
			} else {
				$(obj).append('<span class="loaded">已显示全部</span>');
			}
			_this.callback(obj, oData);
			_this.setLiveAndFollow(obj, oData);
		};
	},
	triggerShow: function(obj, temID) {
		if(!$(obj).attr('loaded')){
			this.ajax_load(obj,temID);
			$(obj).attr('loaded', 'loaded');
		}
	},
	requireSetData: function(json, obj, temID) {
		var trigger = $(obj).attr('data-trigger');
		var tH = json.oH + json.oT + 100;
		var sLoad = $(obj).attr("loaded");
		var id = $(obj).attr("id");
		var _this = this;
		if(!trigger && tH>json.oM && !sLoad){
			_this.ajax_load($(obj),temID);
		}
	},
	setLazyLoadData: function() {
		var h = $(window).height(),
			t = $(window).scrollTop(),
			_this = this;
		$('.ajax-load').each(function(index){
			var me = this,
				mT = $(me).offset().top,
				temId = $(me).attr('temId');
			_this.requireSetData({oH: h, oT: t, oM: mT}, me, temId);
		});
	},
	// 可扩展
	setLiveAndFollow: function(obj, data) {
		var userIds = [],
		live = $(obj).attr('data-live'),
		follow =  $(obj).attr('data-follow');
		if(!live && !follow) return;
        for(var i = 0, l = data.info.length; i < l; i++){
            if(data.info[i].userID) {
            	userIds.push(data.info[i].userID);
            	// 解决七夕活动userID、anchorID同时出现在一个结构体
            	if(data.info[i].anchorID && data.info[i].userID != data.info[i].anchorID) userIds.push(data.info[i].anchorID);
            }
            else userIds.push(data.info[i].userid);
        }
        if(follow) {
	        $.ajax({
	            url: '//music.51vv.com/act/user/getFollowState',
	            data: { users: JSON.stringify(userIds) },
	            success: function (data) {
	                if(data.result == 0) {
	                    for(var i = 0, l = data.followUsers.length; i < l; i++) {
	                    	var t = $(obj).find('.follow[data-userid="' + data.followUsers[i].userId + '"]');
	                        if(data.followUsers[i].isFollow) {
	                        	t.attr('data-followtype', 1);
	                        	t.removeClass('to_followed').addClass('followed').html('已关注');
	                        }
	                        t.show();
	                    }
	                }
	            }
	        });
        }
        if(live) {
	        $.ajax({
	            url: '//music.51vv.com/act/user/getLiveState',
	            data: { users: JSON.stringify(userIds) },
	            success: function (data) {
	                if(data.result == 0) {
	                    for(var i = 0, l = data.liveUsers.length; i < l; i++) {
	                        var t = $(obj).find('.live[data-userid="' + data.liveUsers[i].userId + '"]');
	                        if(data.liveUsers[i].liveId) t.attr('data-liveid', data.liveUsers[i].liveId).show();
	                    }
	                }
	            }
	        });
	    }
	},
	init: function() {
		var _this = this;
		$(window).on("scroll",function(){
			if(clearDir) clearTimeout(clearDir);
			clearDir = setTimeout(function(){
				_this.setLazyLoadData();
			}, 200);
		});
		$(window).on("resize",function(){
			_this.setLazyLoadData();
		});
		_this.setLazyLoadData();
	}
};