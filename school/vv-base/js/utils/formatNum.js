// 格式化数量 num 数量 radix格式化的基数 单位
function formatNum(num,radix,unit) {
    return num > (radix - 1) ? (num / radix).toFixed(1) + unit : num;
}