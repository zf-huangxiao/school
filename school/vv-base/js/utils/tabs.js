/*tabs*/
var clearDirTabs = null;
vv.tabs = {
	getTabs: function(el, me, oType){
		var index = $(el).index(),
			pIndex = $(me).find('.E-tabs-list').eq(index),
			_this = this;
		$('.E-tabs-list', me).addClass('none');
		pIndex.removeClass("none");
		if (pIndex.hasClass('ajax-load')) {
			if (pIndex.attr("data-trigger")) {
				vv.ajax.triggerShow(pIndex, pIndex.attr('temId'));
			}
		} else if (pIndex.find('.ajax-load').length > 0) {
			var ajaxData=pIndex.find('.ajax-load');
			for(var i=0;i<ajaxData.length;i++){
				if($(ajaxData[i]).attr("data-trigger")){
					vv.ajax.triggerShow(ajaxData[i], $(ajaxData[i]).attr('temId'));
				}
			}
		}
		$('.E-tabs-title', me).removeClass(function(i, oldClass) {
			var className = oldClass.match(/E-tabs-title-\d*/g);
			if (className) return oldClass.match(/E-tabs-title-\d*/g).join(' ');
			else return null;
        });
        $('.E-tabs-title', me).addClass('E-tabs-title-' + index);
		$('.E-tabs-title li', me).removeClass('select');
		$(el).addClass('select');
	},
	getAttr: function(me){
		var oType = $(me).attr('event-type'),
			_this = this;
		switch(oType){
			case "hoverEvent":
				$(me).on('mouseenter', '.E-tabs-title li', function(e){
					var el = this;
					if(clearDirTabs) clearTimeout(clearDirTabs);
					clearDirTabs = setTimeout(function(){
						_this.getTabs(el, me, oType);
					}, 200);
					return false;
				});
				break;
			case "clickEvent":
				$(me).on('click', '.E-tabs-title li', function(e){
					var el = this;
					_this.getTabs(el, me, oType);
				});
				$(me).on('click', '.E-tabs-title li a', function(ev){
					if(!$(this).parent().hasClass('select')){
						ev.preventDefault();
					}
				});
				break;
		};

	},
	trigger: function(me){
		var id = $(me).attr('data-id'),
			pIndex = $(id);
		pIndex.removeClass("none");
		if(pIndex.attr("data-trigger")) {
			vv.ajax.triggerShow(pIndex, pIndex.attr('temId'));
		}
	}
};
