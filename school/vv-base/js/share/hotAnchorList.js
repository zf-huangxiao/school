/**
 * 右侧-热门主播列表
 * suyue 2017-07-26
 */
;
(function() {
    var baseHotList = {
        init: function() {
            baseHotList.hotList();
            baseHotList.eventBind();
            // 防止右侧滑动到底部挡住footer内容-小屏幕会出现
            $(document).scroll(function() {
                if ($(document).scrollTop() > 0) {
                    $('.hot_anchor_box').css('top', 'auto');
                } else {
                    $('.hot_anchor_box').css('top', '90px');
                }
            });
        },
        eventBind: function() {
            $('.container').on('click', '.hot_anchor_list .follow_btn', function() {
                $('#downloadPopup').show();
            })
        },
        // 热门主播列表
        hotList: function() {
            $.ajax({
                url: '//music.51vv.com/wx/s/hotlist',
                data: {
                    curPage: 1,
                    viewNumber: 5
                },
                type: 'get',
                dataType: 'json',
                success: function(data) {
                    if (data.result == 0) {
                        if (data.live && data.live.length > 0) {
                            var html2 = '';
                            var liveLen2 = data.live.length;
                            for (var i = 0; i < liveLen2; i++) {
                                html2 += '<li class="clearfix">' +
                                    '<a href="//music.51vv.com/wx/s/?liveid=' + data.live[i].liveID + '&uid=' + data.live[i].userID + '" target="_blank">' +
                                    '<img src="' + baseHotList.formatUserImg(data.live[i].userImg, '_m.jpg') + '" onerror="this.src=\'../m/share_userContent_pc/images/user_img_default.jpg\'"/>' +
                                    '</a>' +
                                    '<div class="nickname_box">' +
                                    '<a href="//music.51vv.com/wx/s/?liveid=' + data.live[i].liveID + '&uid=' + data.live[i].userID + '" target="_blank">' +
                                    '<p class="nickname">' + data.live[i].nickName + '</p>' +
                                    '</a>' +
                                    (function() {
                                        if (data.live[i].description) {
                                            return '<p class="description">' + data.live[i].description + '</p>';
                                        } else {
                                            return '<p class="description">主播啥也没说，看看直播吧！</p>';
                                        }
                                    })() +
                                    '</div>' +
                                    '<span class="follow_btn follow_default_btn"></span>' +
                                    '</li>';
                            }
                            $('#hotAnchorList').html(html2);
                        }
                    }
                }
            });
        },
        //转换图片格式
        formatUserImg: function(url, type) {
            if (url == '') {
                return '../m/share_userContent_pc/images/user_img_default.jpg';
            }
            if (url.indexOf('_') != -1) {
                return url.substr(0, url.indexOf('_')) + type;
            } else {
                return url.substr(0, url.lastIndexOf('.')) + type;
            }
        }
    }
    baseHotList.init();
})();