/**
 * 站外用户信息
 * suyue 2017-07-26
 */
;
(function() {
    var local = {
        init: function() {
            local.eventBind();
            local.contentInfo();
            $('.user_wrap .icon_level').attr('backgroundImage', 'url(' + local.levelFn(window.msgObj.userSimpleInfo) + ')');
        },
        eventBind: function() {
            $('.user_wrap .focus_btn').on('click', function() {
                $('#downloadPopup').show();
            });

        },
        // 用户vip，贵族，等级图
        levelFn: function(userSimpleInfo) {
            if (!userSimpleInfo) return;
            var levelUrl = '';
            if (userSimpleInfo.vip && userSimpleInfo.vip.length > 0) {
                levelUrl = local.getSmallVipLevel(userSimpleInfo.vip[0]);
            } else if (userSimpleInfo.vipImgUrl) {
                levelUrl = userSimpleInfo.vipImgUrl;
            } else if (userSimpleInfo.levelImgUrl) {
                levelUrl = userSimpleInfo.levelImgUrl;
            }
            return levelUrl;

        },
        // 获取贵族url地址
        getSmallVipLevel: function(level) {
            $.ajax({
                url: '//music.51vv.com/user/cfg/getCfgInfo.htm',
                success: function(data) {
                    if (data.result == 0 && data.familyVipImgPrefix) {
                        var getVipLevelUrl = data.familyVipImgPrefix;
                        return getVipLevelUrl + level + '_pc_small.png';
                    }
                }
            })
        },
        // 获取内容详情，解决增加次数+1
        contentInfo: function() {
            $.ajax({
                url: '//music.51vv.com/wx/s/getUserContent.htm?contentId=' + window.msgObj.contentId,
                success: function(data) {
                    console.log(data);
                }
            })
        }



    }
    local.init();
})();