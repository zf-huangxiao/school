/**
 * 分享 置顶
 * suyue
 */
var baseUrl = 'https://music.51vv.com/wx/mp/mp_' + window.msgObj.contentId + '.htm';
var baseShare = {
    init: function() {
        baseShare.eventBind();
        // 二维码
        var payCodePrm = {
            render: "canvas",
            background: "#ffffff",
            foreground: "#000000",
            width: 74, //宽度 
            height: 74, //高度 
            typeNumber: -1,
            correctLevel: 0, //纠错等级  
            text: baseUrl //二维码url地址
        }
        $(".wechat_code")[0].appendChild(createCanvas(payCodePrm));

    },
    eventBind: function() {
        //置顶
        window.onwheel = function() {
            //页面已滚动的高度
            var scrollHeight = document.documentElement.scrollTop || document.body.scrollTop;
            //窗口的高度
            var windowHeight = document.documentElement.clientHeight || document.body.clientHeight;
            if (windowHeight < scrollHeight) {
                $('.top_btn').show();
            } else {
                $('.top_btn').hide();
            }
        };
        //置顶
        $('.top_btn').off('click').on('click', function() {
            $('body').scrollTop(0);
        });
        $('.base_web_share .share-list-btn').off('click').on('click', function() {
            var shareType, shareNick, shareImg;
            shareType = $(this).data('type');
            // 分享参数 
            var para = {
                shareType: shareType,
                shareNick: $('.user_wrap .nikName').html() + '_VV音乐资讯',
                sharDesc: baseUrl,
                shareImg: window.msgObj.coverpic
            }
            baseShare.shareCallback(para);
        });
    },
    // 分享
    shareCallback: function(data) {
        var shareUrl = '';
        switch (data.shareType) {
            case 'qq':
                shareUrl = 'http://connect.qq.com/widget/shareqq/index.html?url=' + encodeURIComponent(baseUrl) + '&title=' + encodeURIComponent(data.shareNick) + '&summary=' + encodeURIComponent(data.sharDesc) + '&desc=&site=&pics=' + encodeURIComponent(data.shareImg);
                break;
            case 'sina':
                shareUrl = 'http://service.weibo.com/share/share.php?url=' + encodeURIComponent(baseUrl) + '&title=' + encodeURIComponent(data.shareNick) + '&pic=' + encodeURIComponent(data.shareImg);
                break;
            case 'qqzone':
                shareUrl = 'http://sns.qzone.qq.com/cgi-bin/qzshare/cgi_qzshare_onekey?url=' + encodeURIComponent(baseUrl) + '&title=' + encodeURIComponent(data.shareNick) + '&summary=""&site&pics=' + encodeURIComponent(data.shareImg);
                break;
            case 'wechat':
                shareUrl = baseUrl;
                break;
            case 'copyLink':
                common.shareCopyLink(baseUrl);
                break;
            default:
                break;
        }
        if (data.shareType == 'wechat') {
            $(".wechat_code").show();
        } else {
            window.open(shareUrl);
        }
    }

}

baseShare.init();