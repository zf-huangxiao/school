//把对象生成url参数
window.toParameter = function(obj) {
    var parameter = '';
    for (var key in obj) {
        parameter = parameter + key + '=' + obj[key] + '&';
    }
    parameter = parameter.substring(0, parameter.length - 1);
    return '?' + parameter;
};
var getVipLevelUrl;
var getSmallVipUrl;
// 配置图片地址
function getCfgInfo() {
    var getCfgInfoObj = {
        type: 'https://music.51vv.com/user/cfg/getCfgInfo.htm',
        parameter: '',
        base: location.href,
        callback: 'getCfgInfoCallback'
    };
    window.getCfgInfoCallback = function(data) {
        if (data.result == 0 && data.familyVipImgPrefix) {
            getVipLevelUrl = data.familyVipImgPrefix;
        }
    }
    if(typeof QCefClient != 'undefined'){
       QCefClient.invokeMethod('server', JSON.stringify(getCfgInfoObj));

    }
}
getCfgInfo()
    //公共函数
window.common = {
    // 分享 复制链接 copyobject 隐藏的input标签 _shareUrl 分享的链接地址，sucobject 分享成功后的弹窗提示
    shareCopyLink: function(_shareUrl, sucobject) {
        if (!sucobject) {
            sucobject = $('.popup_copyLink');
        }
        var copyobject = document.getElementById("copy-content");
        copyobject.value = _shareUrl;
        copyobject.select();
        document.execCommand("Copy");
        // 复制链接成功 弹窗
        sucobject.show();
        setTimeout(function() {
            sucobject.hide();
        }, 2000);
    },
    //生成guid
    guid: function() {
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
            var r = Math.random() * 16 | 0,
                v = c == 'x' ? r : (r & 0x3 | 0x8);
            return v.toString(16);
        });
    },
    //生成认证图标
    getSign: function(level) {
        var lev = +level;
        var zoom = [17, 33, 49, 65, 81, 97, 113, 129, 145, 161, 177, 193, 209, 225, 241, 257, 273, 289, 305, 321, 337, 353, 369, 385];
        var img = '';
        for (var i = 0; i < zoom.length; i++) {
            if (lev >= zoom[zoom.length - 1]) {
                img = 'http://cdn.file.51vv.com/vvmobilelive/pc/ico/385-400.png';
                break;
            }
            if (lev >= zoom[i] && lev < zoom[i + 1]) {
                img = 'http://cdn.file.51vv.com/vvmobilelive/pc/ico/' + zoom[i] + '-' + --zoom[i + 1] + '.png';
                break;
            }
        }
        return img;
    },
    //生成图标
    getLevel: function(level) {
        return 'http://cdn.file.51vv.com/vvmobilelive/pc/level/' + level + '.png';
    },
    //生成贵族图标
    getVipLevel: function(level) {
        return getVipLevelUrl + level + '_pc.png';
    },
    //生成小的贵族图标
    getSmallVipLevel: function(level) {
        return getVipLevelUrl + level + '_pc_small.png';
    },
    //生成图片地址
    getUserImg: function(url) {
        if (!url.match(/\.webp/ig)) {
            url.replace(/\.\w+$/ig, '_t.webp')
        }
        return url;
    },
    // 特殊字符
    formatStr: function(str) {
        return str.replace(/&/g, '&amp;').replace(/\s/g, '&nbsp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
    },
    //获取url参数
    getUrlParam: function(name) {
        var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)"); //构造一个含有目标参数的正则表达式对象
        var r = window.location.search.substr(1).match(reg); //匹配目标参数
        if (r != null) {
            return r[2];
        } else {
            return null; //返回参数值
        }
    },

    //滚动加载绑定
    loadPage: function(dom, callback, params) {
        dom.onscroll = sLoad;

        function sLoad() {
            //判断如果 最下面的loading距离窗口顶部的距离 + 页面已滚动的高度 < 页面已滚动的高度 + 窗口的高度
            //内部页面的高度
            var scrollHeight = dom.scrollHeight;
            // 页面上部隐藏的高度
            var scrollTop = dom.scrollTop;
            //对外可视的高度
            var clientHeight = dom.clientHeight;

            if (scrollHeight - 400 < scrollTop + clientHeight) {
                //解除window.onscroll事件绑定，否则活动到loading区域中时会多次执行，等到加载完下一屏的数据后再绑定上
                dom.onscroll = null;
                //到达底部后延迟500毫秒再渲染下一屏
                setTimeout(function() {
                    //加载内容
                    callback(params); //
                    dom.onscroll = sLoad;
                }, 1000);
            }
        }
    },
    //滚动加载绑定
    loadList: function(dom, callback, height, params) {
        dom.onscroll = sLoad;

        function sLoad() {
            //判断如果 最下面的loading距离窗口顶部的距离 + 页面已滚动的高度 < 页面已滚动的高度 + 窗口的高度
            //内部页面的高度
            var scrollHeight = dom.scrollHeight;
            // 页面上部隐藏的高度
            var scrollTop = dom.scrollTop;
            //对外可视的高度
            var clientHeight = dom.clientHeight;

            if (dom.tagName == 'BODY') {
                clientHeight = document.documentElement.clientHeight;
            }

            if (scrollHeight - (height || 400) < scrollTop + clientHeight) {
                //解除window.onscroll事件绑定，否则活动到loading区域中时会多次执行，等到加载完下一屏的数据后再绑定上
                dom.onscroll = null;
                //加载内容
                callback(params);
            }
        }
    },
    //时间格式化
    getTime: function(formart, time, type) {
        //yyyy 	将年份表示为四位数字。如果少于四位数，前面补零。
        //MM 	将月份表示为从 1 至 12 的数字，如果小于两位数，前面补零。
        //dd 	将月中日期表示为从 1 至 31 的数字，如果小于两位数，前面补零。
        //HH 	将小时表示为从 0 至 23 的数字，如果小于两位数，前面补零。
        //mm 	将分钟表示为从 0 至 59 的数字，如果小于两位数，前面补零。
        //ss 	将秒表示为从 0 至 59 的数字，如果小于两位数，前面补零。
        //如果为yy，M，d，H，m，s则不补0。
        var time = time || new Date().getTime();
        if (typeof type === 'number' || typeof type === 'string') {
            time = new Date(time + (+type));
        }
        var fullyear = time.getFullYear();
        var month = (time.getMonth() + 1) < 10 ? "0" + (time.getMonth() + 1) : (time.getMonth() + 1);
        var date = time.getDate() < 10 ? "0" + time.getDate() : time.getDate();
        var hours = time.getHours() < 10 ? "0" + time.getHours() : time.getHours();
        var minutes = time.getMinutes() < 10 ? "0" + time.getMinutes() : time.getMinutes();
        var seconds = time.getSeconds() < 10 ? "0" + time.getSeconds() : time.getSeconds();
        if (type === 'start') {
            date = '01';
            hours = '00';
            minutes = '00';
            seconds = '00';
        }
        if (type === 'end') {
            time = new Date(time.getFullYear(), (time.getMonth() + 1), 1, 0, 0, 0);
            time = time.getTime() - 24 * 60 * 60 * 1000;
            date = time.getDate() < 10 ? "0" + time.getDate() : time.getDate();
            hours = '23';
            minutes = '59';
            seconds = '59';
        }
        return formart.replace('%yyyy', fullyear).replace('%MM', month).replace('%dd', date).replace('%HH', hours).replace('%mm', minutes).replace('%ss', seconds).replace('%yy', (fullyear + '').substr(2)).replace('%M', +month).replace('%d', +date).replace('%H', +hours).replace('%m', +minutes).replace('%s', +seconds);
    },
    //历史时间
    historicalTime: function(time) {
        var now = {};
        now.Y = new Date().getFullYear();
        now.M = new Date().getMonth();
        now.D = new Date().getDate();
        var old = new Date(time).getTime();
        if (old >= new Date(now.Y, now.M, now.D).getTime()) {
            //当天
            return common.getTime('%HH:%mm', new Date(old));
        } else if (old >= new Date(now.Y, now.M, now.D - 1).getTime()) {
            //昨天
            return '昨天';
        } else if (old >= new Date(now.Y, 1, 1).getTime()) {
            //今年
            return common.getTime('%MM-%dd', new Date(old));
        } else {
            //更早
            return common.getTime('%yyyy-%MM-%dd', new Date(old));
        }
    },
    //手机跳转
    toMobile: function(url) {
        var sUserAgent = navigator.userAgent.toLowerCase();
        var bIsIpad = sUserAgent.match(/ipad/i) == "ipad";
        var bIsIphoneOs = sUserAgent.match(/iphone os/i) == "iphone os";
        var bIsMidp = sUserAgent.match(/midp/i) == "midp";
        var bIsUc7 = sUserAgent.match(/rv:1.2.3.4/i) == "rv:1.2.3.4";
        var bIsUc = sUserAgent.match(/ucweb/i) == "ucweb";
        var bIsAndroid = sUserAgent.match(/android/i) == "android";
        var bIsCE = sUserAgent.match(/windows ce/i) == "windows ce";
        var bIsWM = sUserAgent.match(/windows mobile/i) == "windows mobile";
        var isM = sUserAgent.match(/iphone/gi) == "iphone";
        var isvvlive = /vvlive/gi.test(sUserAgent);
        if (bIsIpad || bIsIphoneOs || bIsMidp || bIsUc7 || bIsUc || bIsAndroid || bIsCE || bIsWM || isM || isvvlive) {
            window.location.href = url;
        }
    }
};

//等待loadVVLive加载成功后调用
$(function() {
    var t = setInterval(function() {
        if (window.loadVVLive) {
            clearInterval(t);
            window.loadVVLive(jQuery);
        }
    }, 200);
});
