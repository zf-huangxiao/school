﻿(function(window,$){
    scrollF = function(_scrollId,_number,_time,_content){
        //当前页数
        this.pageId = 0;
        //当前滚动条id
        this.scrollId = _scrollId;
        //页面显示个数
        this.number = _number;
        //滚动宽度
        this.width = $('#picBox' + this.scrollId).width();
        //滚动时间
        this.time = _time;
        //是否存在内容
        this.content = _content || false;
        //页面总数
        this.totalPage = $('#picBox' + this.scrollId + ' ul li').size()/this.number;
        if ($('#picBox' + this.scrollId + ' ul li').length == 1){
            $('#picPrev' + this.scrollId).css('display','none');
            $('#picNext' + this.scrollId).css('display','none');
        }
        $('#picPrev' + this.scrollId).attr('href','javascript:scrollObj' + this.scrollId + '.picPrev()');
        $('#picNext' + this.scrollId).attr('href','javascript:scrollObj' + this.scrollId + '.picNext()');
    };
    scrollF.prototype = {
        //上一页
        picPrev: function () {
            this.pageId--;
            if (this.pageId <= 0){
                this.pageId = 0;
                $('#picPrev' + this.scrollId).css('display','none');
            }
            $('#picBox' + this.scrollId).stop().animate({"scrollLeft": this.pageId * this.width}, this.time);
            $('#picNext' + this.scrollId).css('display','block');
        },
        //下一页
        picNext: function () {
            this.pageId++;
            if (this.pageId >= this.totalPage - 1){
                this.pageId = this.totalPage - 1;
                $('#picNext' + this.scrollId).css('display','none');
            }
            $('#picBox' + this.scrollId).stop().animate({"scrollLeft": this.pageId * this.width}, this.time);
            $('#picPrev' + this.scrollId).css('display','block');
        },
        //返回起始位置
        reset: function () {
            this.pageId = 0;
            $('#picPrev' + this.scrollId).css('display','none');
            $('#picBox' + this.scrollId).stop().animate({"scrollLeft": this.pageId * this.width}, this.time);
            $('#picNext' + this.scrollId).css('display','block');
        }
    };
})(window,jQuery);
